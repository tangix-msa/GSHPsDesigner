"""
    FLS_III(N::Int, M::Int, H::T, D::T, B::T, tmax::Int; tstep::T = 8760. * 3600, k::T=3.1, ro::T = 2300. , cp::T = 870., rb::T = 0.0575 , m::Int=12) where {T<:AbstractFloat}

Evaluate the thermal response of a borehole field with parallel connections.

# Outputs

1.  `x`: non-dimensional temperature change evolution (to be multiplied by the borehole load and divided by 2*pi*k to obtain the dimensional temperature change evolution) and load profile of the boreholes.

# Arguments

1. `N`: number of boreholes on one side of the rectangular grid [-]
2. `M`: number of boreholes on the other side of the grid [-]
3. `H`: length of the boreholes [m]
4. `D`: buried depth [m]
5. `B`: distance between consecutive boreholes [m]
6. `tmax`: final timestep at which to calculate the g function [year]

## Keyword arguments
- `tstep::T = 8760. * 3600`: length of each times step at which the solution is calcualted [s]
- `k = 3.1`:  ground thermal conductivity [W/mK]
- `ro = 2300`: ground density [kg/m3]
- `cp = 870`: ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`: borehole radius [m]
- `m::Int=12`: number of segments per borehole [-]

Implementation of the FLS model with BC III: uniform temperature along the borehole field.
"""
function FLS_III(N::Int, M::Int, H::T, D::T, B::T, tmax::Int, tstep::T; k::T=3.1, ro::T = 2300. , cp::T = 870., rb::T = 0.0575 , m::Int=12) where {T<:AbstractFloat}
    αg = k/ro/cp;   # ground thermal diffusivity [m2/s]

    t = collect(tstep : tstep : tstep * tmax);  # time [s] at which the g function is calculated.
    steps = length(t);

    n_boreholes = N * M;
    n_segments = n_boreholes * m;
    Hseg = H/m;                                         # [m] lenght of each borehole segment

    Psrc = prism(B, N, M, D, H, m);                     # position of the segments
    positions = rectangle(B,N,M);

    h = fill(0., n_segments, n_segments, steps);
    for ii = 1 : n_segments                             ## ii index of the affected borehole
    	for jj = 1 : n_segments                         ## jj index of the heat source borehole
    	   h[ii,jj,:] = fls(t, αg, max(euclidean(positions[Int64(ceil(ii/m))],positions[Int64(ceil(jj/m))]),rb), Psrc[jj][3], Psrc[ii][3], Hseg, Hseg,);
        end
    end

	A1 = fill(0., 1, 1);
	A2 = fill(Hseg, 1, n_segments);
	A3 = fill(-1., n_segments, 1);
	A4 = h[:,:,1];
	AA = vcat(hcat(A1,A2),hcat(A3, A4));
	A_inv= inv(AA);

	b0 = H * n_boreholes;                               # total field load at 1 W/m
	b = vcat(b0, fill(0., n_segments));

    x = fill(0., 1 + n_segments, steps);                # solution [T, incremental load profile]
	x[:,1] = A_inv * b;

    for TT = 2 : steps
        convolution = fill(0., n_segments, 1);
        for kk = 1 : TT-1
            convolution = convolution .+ (h[:,:,TT-(kk-1)]  * x[2:end,kk])
        end
        b = vcat(0., - convolution);
        x[:,TT] = A_inv * b;
    end
return x
end # FLS_III(N::Int, M::Int, H::T, D::T, B::T, tstep::T = 8760.* 3600; tmax::Int; k::T=3.1, ro::T = 2300 , cp::T = 870, rb::T = 0.0575 , m::Int=12 ) where {T<:AbstractFloat}
"""
    FLS_III(field_characteristics::Array{T,2}, tmax::Int; tstep::T = 8760. * 3600, k::T=3.1, ro::T = 2300. , cp::T = 870., rb::T = 0.0575 , m::Int=12) where {T<:AbstractFloat}

Evaluate the thermal response of a borehole field with parallel connections.

# Outputs:

1.  `x`: non-dimensional temperature change evolution (to be multiplied by the borehole load and divided by 2*pi*k to obtain the dimensional temperature change evolution) and load profile of the boreholes.

# Arguments


1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field [xpos, ypos, D, H]
2. `tmax`: final timestep at which to calculate the g function [year]

## Keyword arguments
- `tstep::T = 8760. * 3600`: length of each times step at which the solution is calcualted [s]
- `k = 3.1`:  ground thermal conductivity [W/mK]
- `ro = 2300`: ground density [kg/m3]
- `cp = 870`: ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`: borehole radius [m]
- `m::Int=12`: number of segments per borehole [-]

Implementation of the FLS model with BC III: uniform temperature along the borehole field.
"""
function FLS_III(field_characteristics::Array{T,2}, tmax::Int; tstep::T = 8760. * 3600, k::T=3.1, ro::T = 2300. , cp::T = 870., rb::T = 0.0575 , m::Int=12) where {T<:AbstractFloat}
    αg = k/ro/cp;   # ground thermal diffusivity [m2/s]

    t = collect(tstep : tstep : 3600. * 8760. * tmax);  # time [s] at which the g function is calculated.
    steps = length(t);

    n_boreholes = length(field_characteristics[:,1]);
    n_segments = n_boreholes * m;

    z_matrix = fill(0., n_boreholes, m)
    for ii = 1 : n_boreholes
        z_matrix[ii,:] = collect(field_characteristics[ii,3] : field_characteristics[ii,4]/m : field_characteristics[ii,3]+(m-1)*field_characteristics[ii,4]/m*100.1/100)  #*100.1/100 is added for computational reason
    end
    G3 = fill(Point3{Float64}(0.,0.,0.), n_segments);
    for ii = 1 : n_boreholes
        for jj = 1:m
            G3[(ii-1)*m+jj] = Point3{Float64}(field_characteristics[ii,1],field_characteristics[ii,2],z_matrix[ii,jj])
        end
    end

    distances = fill(0.,n_boreholes,n_boreholes);
    for ii = 1: n_boreholes
        for jj = 1:n_boreholes
            distances[ii,jj] = euclidean(field_characteristics[ii,1:2],field_characteristics[jj,1:2]);
        end
    end
    h = fill(0., n_segments, n_segments, steps);
    for ii = 1 : n_segments
        ii_subindex = Int64(ceil(ii/m));
        for jj = 1 : n_segments
            jj_subindex = Int64(ceil(jj/m));
            h[ii,jj,:] = fls(t, αg, max(distances[ii_subindex,jj_subindex],rb), G3[jj][3], G3[ii][3], field_characteristics[Int64(ceil(jj/m)),4]/m, field_characteristics[Int64(ceil(ii/m)),4]/m);
        end
    end
    # Building matrix A_____________________________________________________________
        A1 = fill(0., 1,1);
        A2 = fill(0., 1, n_segments);
        A3 = fill(-1., n_segments, 1);
        for zz = 1 : n_boreholes
            for jj = (zz-1) * m + 1: (zz) * m
                A2[1,jj] = field_characteristics[zz,4]/m;        # construction matrix A2
            end
        end
        A4 = h[:,:,1]
        AA = vcat(hcat(A1, A2),hcat(A3,A4));
        A_inv= inv(AA);
    # end (Building matrix A)_____________________________________________________

    # Initialization of the solution vector_________________________________________
        x = fill(0., 1 + n_segments, steps);
    # end (Initialization of the solution vector)___________________________________

        b0 = sum(field_characteristics[:,4]);                               # total field load at 1 W/m
    	b = vcat(b0, fill(0., n_segments));

        x[:,1] = A_inv * b;

        for TT = 2 : steps
            convolution = fill(0., n_segments, 1);
            for kk = 1 : TT-1
                convolution = convolution .+ (h[:,:,TT-(kk-1)]  * x[2:end,kk])
            end
            b = vcat(0., - convolution);
            x[:,TT] = A_inv * b;
        end

        return x
    end
