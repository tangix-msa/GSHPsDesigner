"""
    FLS_I(borehole_characteristics::Array{T,1}, tmax::Int; T_ground = 8., tstep::T=3600., k::T=3.1, ro::T=2300., c_p::T=870., rb::T=5.75e-2) where{T<:AbstractFloat}

    Evaluate the **borehole wall temperature** of an isolated borehole.
    Implementation of the FLS model with BC I: uniform heat load along the boreholes.

# Output:

`temperature`: borehole wall temperature [°C]

# Arguments:

1. `borehole_characteristics`: vector containing the following characteristics for the borehole [D: buried depth [m],H: total borehole length [m] , yearly borehole load (positive means extraction) [kWh]]
2. `tmax: final`: number of time steps to simulate

## Keyword arguments (optional):

- `T_ground = 8`        # initial ground temperature [°C]
- `tstep = 3600`;       # length of each time step [s]
- `k = 3.1`;            # ground thermal conductivity [W/mK]
- `ro = 2300`;          # ground density [kg/m3]
- `cp = 870`;           # ground heating capacity [J/(kg K)]
- `rb = 0.0575`;        # borehole radius [m]
"""
function FLS_I(borehole_characteristics::Array{T,1}, tmax::Int; T_ground = 8., tstep::T=3600., k::T=3.1, ro::T=2300., c_p::T=870., rb::T=5.75e-2) where{T<:AbstractFloat}

    αg = k/ro/c_p; # ground thermal diffusivity [m2/s]
    t = collect(1.:tmax) .* tstep;  # [s] time at which the solutions are evaluated. Must be in seconds!!
    length = borehole_characteristics[2] .- borehole_characteristics[1];

    h_FLS = fls(t, αg, rb, borehole_characteristics[1], borehole_characteristics[1], length, length);

    linear_load = borehole_characteristics[3] ./ length * 1000/8760; # load in W/m
    x =  h_FLS .* linear_load;
    temperature = T_ground .- x ./ (2*pi*k);

    return temperature
end

"""
    FLS_I(field_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}

Evaluate the **borehole walls temperatures** in a neighbourhood with independent ground source heat pump installations.
Implementation of the FLS model with BC I: uniform heat load along the boreholes.

# Output:

1. `temperature`: temperature  of all the installations [°C]

# Arguments:

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `tmax: final`: timestep at which to calculate the temperature [year if the default timestep is used]

## Keyword arguments (optional):

- `T_ground = 8`        # initial ground temperature [°C]
- `tstep = 8760*3600`;  # length of each time step [s]
- `k = 3.1`;            # ground thermal conductivity [W/mK]
- `ro = 2300`;          # ground density [kg/m3]
- `c_p = 870`;           # ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`;        # borehole radius [m]
"""
function FLS_I(field_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., c_p::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}
    if size(field_characteristics)[2] != 6
        println("The matrices field_characteristics and new_borehole_characteristics should have 6 coloumns");
    elseif tmax < 0
        println("tmax must be a positive number");
    else

    αg = k/ro/c_p; # ground thermal diffusivity [m2/s]

    n_existing_boreholes = length(field_characteristics[:,1]);

    t = collect(1.:tmax) .* tstep;  # [s] time at which the solutions are evaluated. Must be in seconds!!
    last_year_simulated = tmax + maximum(field_characteristics[:,1]);
    years_extra_existing_boreholes = maximum(field_characteristics[:,1]) .- field_characteristics[:,1]

    distances = fill(0.,n_existing_boreholes, n_existing_boreholes) ;
    for ii = 1 : n_existing_boreholes
        for jj = 1 : n_existing_boreholes
            distances[ii,jj] = euclidean(field_characteristics[ii,2:3], field_characteristics[jj,2:3]);
        end
    end

    lengths = field_characteristics[:,5] .- field_characteristics[:,4];
    steps = Int64(tmax);
    h_FLS = fill(0., n_existing_boreholes , n_existing_boreholes , steps);
    for ii = 1 : n_existing_boreholes
        for jj = 1 : n_existing_boreholes
            h_FLS[ii,jj,:] = fls(t .+ years_extra_existing_boreholes[jj]*8760*3600, αg, max(distances[ii,jj],rb), field_characteristics[jj,4], field_characteristics[ii,4], lengths[jj], lengths[ii]);
        end
    end

    linear_loads = field_characteristics[:,6] ./ lengths * 1000/8760; # load in W/m
    x = fill(0., n_existing_boreholes, steps);

    for TT = 1:steps
        x[:, TT] =  h_FLS[:,:,TT] * linear_loads;
    end
    temperature = T_ground .- x ./ (2*pi*k);

    return temperature
end
    end
"""
    FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}

Evaluate the **borehole walls temperatures** in a neighbourhood with independent ground source heat pump installations.
Implementation of the FLS model with BC I: uniform heat load along the boreholes.

# Output:

1. `temperature_with_new_installation`: temperature  of all the installations taking into account the new installation (The first rows contain the results for the existing installations in the same order as the input given to the function. The last row contains the results for the new installation) [°C]
2. `temperature_without_new_installation`: temperature without the new installation [°C]
3. `temperature_new_borehole_if_undisturbed`: temperature of the new installation if isolated [°C]

# Arguments:

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `tmax: final`: timestep at which to calculate the temperature [year if the default timestep is used]

## Keyword arguments (optional):

- `T_ground = 8`        # initial ground temperature [°C]
- `tstep = 8760*3600`;  # length of each time step [s]
- `k = 3.1`;            # ground thermal conductivity [W/mK]
- `ro = 2300`;          # ground density [kg/m3]
- `cp = 870`;           # ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`;        # borehole radius [m]
"""
function FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}
    if size(field_characteristics)[2] != 6 || size(new_borehole_characteristics)[2] != 6
        println("The matrices field_characteristics and new_borehole_characteristics should have 6 coloumns");
    elseif tmax < 0
        println("tmax must be a positive number");
    else

    αg = k/ro/cp; # ground thermal diffusivity [m2/s]

    n_existing_boreholes = length(field_characteristics[:,1]);
    complete_field_characteristics = vcat(field_characteristics, new_borehole_characteristics);
    n_total_boreholes = length(complete_field_characteristics[:,1]);

    t = collect(1.:tmax) .* tstep;  # [s] time at which the solutions are evaluated. Must be in seconds!!
    last_year_simulated = tmax + new_borehole_characteristics[1]
    years_extra_existing_boreholes = new_borehole_characteristics[1] .- field_characteristics[:,1]

    distances = fill(0.,n_total_boreholes ,n_total_boreholes) ;
    for ii = 1 : n_total_boreholes
        for jj = 1 : n_total_boreholes
            distances[ii,jj] = euclidean(complete_field_characteristics[ii,2:3], complete_field_characteristics[jj,2:3]);
        end
    end

    lengths = complete_field_characteristics[:,5] .- complete_field_characteristics[:,4];
    steps = Int64(tmax);
    h_FLS = fill(0., n_total_boreholes , n_total_boreholes , steps);
    for ii = 1 : n_total_boreholes
        for jj = 1 : n_total_boreholes
            if jj <= n_existing_boreholes
                h_FLS[ii,jj,:] = fls(t .+ years_extra_existing_boreholes[jj]*8760*3600, αg, max(distances[ii,jj],rb), complete_field_characteristics[jj,4], complete_field_characteristics[ii,4], lengths[jj], lengths[ii]);
            else
                h_FLS[ii,jj,:] = fls(t, αg, max(distances[ii,jj],rb), complete_field_characteristics[jj,4], complete_field_characteristics[ii,4], lengths[jj], lengths[ii]);
            end
        end
    end

    linear_loads = complete_field_characteristics[:,6] ./ lengths * 1000/8760; # load in W/m
    x_with_new_installation = fill(0., n_total_boreholes, steps);
    x_without_new_installation = fill(0., n_existing_boreholes, steps);
    x_new_borehole_if_undisturbed  = fill(0., 1, steps);
    for TT = 1:steps
        x_with_new_installation[:,TT] =  h_FLS[:,:,TT] * linear_loads;
        x_without_new_installation[:,TT] = h_FLS[1:n_existing_boreholes,1:n_existing_boreholes,TT] * linear_loads[1:n_existing_boreholes];
        x_new_borehole_if_undisturbed[TT]  = h_FLS[end,end,TT] * linear_loads[n_existing_boreholes+1];
    end
    temperature_with_new_installation = T_ground .- x_with_new_installation ./ (2*pi*k);
    temperature_without_new_installation = T_ground .- x_without_new_installation ./ (2*pi*k);
    temperature_new_borehole_if_undisturbed = T_ground .- x_new_borehole_if_undisturbed ./ (2*pi*k);
    return temperature_with_new_installation, temperature_without_new_installation, temperature_new_borehole_if_undisturbed
    end
end # FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2,) where{T<:AbstractFloat}
"""
    FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}

Evaluate the **borehole walls temperatures** in a neighbourhood with independent ground source heat pump installations.
Implementation of the FLS model with BC I: uniform heat load along the boreholes.

# Output:

1. `temperature_with_new_installation`: temperature  of all the installations taking into account the new installation (The first rows contain the results for the existing installations in the same order as the input given to the function. The last row contains the results for the new installation) [°C]
2. `temperature_without_new_installation`: temperature without the new installation [°C]
3. `temperature_new_borehole_if_undisturbed`: temperature on the new installation if isolated [°C]
4.  `temperature_with_all_installations`: temperature taking into account also the possible future installations [°C]

# Arguments:

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `tmax: final`: timestep at which to calculate the temperature [year]
4. `new_field_characteristics`: matrix containing on each row the following characteristics for 1 possible future borehole in the field: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
    ** this value will actually be ignored as it is assumed that all the future boreholes wll be installed at the same time as the borehole under design

## Keyword arguments (optional):

- `T_ground = 8`        # initial ground temperature [°C]
- `tstep = 8760*3600`;  # length of each time step [s]
- `k = 3.1`;      # ground thermal conductivity [W/mK]
- `ro = 2300`;    # ground density [kg/m3]
- `cp = 870`;     # ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`;  # borehole radius [m]
"""
function FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}
    if size(field_characteristics)[2] != 6 || size(new_borehole_characteristics)[2] != 6 || size(new_field_characteristics)[2] != 6
        println("The matrices field_characteristics, new_borehole_characteristics and new_field_characteristics should have 6 coloumns");
    elseif tmax < 0
        println("tmax must be a positive number");
    else

    αg = k/ro/cp; # ground thermal diffusivity [m2/s]

    n_existing_boreholes = length(field_characteristics[:,1]);
    n_future_boreholes = length(new_field_characteristics[:,1]);
    complete_field_characteristics = vcat(field_characteristics, new_borehole_characteristics, new_field_characteristics);
    n_total_boreholes = length(complete_field_characteristics[:,1]);

    t = collect(1.:tmax) .* tstep;  # [s] time at which the solutions are evaluated. Must be in seconds!!
    last_year_simulated = tmax + new_borehole_characteristics[1]
    years_extra_existing_boreholes = new_borehole_characteristics[1] .- field_characteristics[:,1]

    distances = fill(0.,n_total_boreholes ,n_total_boreholes) ;
    for ii = 1 : n_total_boreholes
        for jj = 1 : n_total_boreholes
            distances[ii,jj] = euclidean(complete_field_characteristics[ii,2:3], complete_field_characteristics[jj,2:3]);
        end
    end

    lengths = complete_field_characteristics[:,5] .- complete_field_characteristics[:,4];
    steps = Int64(tmax);
    h_FLS = fill(0., n_total_boreholes , n_total_boreholes , steps);
    for ii = 1 : n_total_boreholes
        for jj = 1 : n_total_boreholes
            if jj<= n_existing_boreholes
                h_FLS[ii,jj,:] = fls(t .+ years_extra_existing_boreholes[jj]*8760*3600, αg, max(distances[ii,jj],rb), complete_field_characteristics[jj,4], complete_field_characteristics[ii,4], lengths[jj], lengths[ii]);
            else
                h_FLS[ii,jj,:] = fls(t, αg, max(distances[ii,jj],rb), complete_field_characteristics[jj,4], complete_field_characteristics[ii,4], lengths[jj], lengths[ii]);
            end
        end
    end

    linear_loads = complete_field_characteristics[:,6] ./ lengths * 1000/8760; # load in W/m
    x_with_all_installations = fill(0.,n_total_boreholes, steps);
    x_with_new_installation = fill(0.,n_existing_boreholes + 1, steps);
    x_without_new_installations = fill(0.,n_existing_boreholes, steps);
    x_new_borehole_if_undisturbed  = fill(0.,1, steps);
    for TT = 1 : steps
        x_with_all_installations[:,TT] =  h_FLS[:,:,TT] * linear_loads;
        x_with_new_installation[:,TT] = h_FLS[1:n_existing_boreholes+1, 1:n_existing_boreholes+1,TT] * linear_loads[1:n_existing_boreholes+1];
        x_without_new_installations[:,TT] = h_FLS[1:n_existing_boreholes,1:n_existing_boreholes,TT] * linear_loads[1:n_existing_boreholes];
        x_new_borehole_if_undisturbed[TT]  = h_FLS[end,end,TT] * linear_loads[n_existing_boreholes+1];
    end
    temperature_with_all_installations = T_ground .- x_with_all_installations ./ (2*pi*k)
    temperature_with_new_installation = T_ground .- x_with_new_installation ./ (2*pi*k);
    temperature_without_new_installation = T_ground .- x_without_new_installations ./ (2*pi*k);
    temperature_new_borehole_if_undisturbed = T_ground .- x_new_borehole_if_undisturbed ./ (2*pi*k);
    return temperature_with_new_installation, temperature_without_new_installation, temperature_new_borehole_if_undisturbed, temperature_with_all_installations
    end
end # FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}
"""
    FLS_I(file_name::String)

Evaluate the **borehole walls temperatures** in a neighbourhood with independent ground source heat pump installations.
Implementation of the FLS model with BC I: uniform heat load along the boreholes.

# Output:

1. `temperature_with_new_installation`: borehole temperature evolution of all the installations taking into account the new installation (The first rows contain the results for the existing installations in the same order as the input given to the function. The last row contains the results for the new installation) [°C]
2. `temperature_without_new_installation`: borehole temperature evolution without the new installation [°C]
3. `temperature_new_borehole_if_undisturbed`: borehole temperature evolution on the new installation if isolated [°C]
4. `temperature_with_all_installations`: borehole temperature evolution taking into account also the possible future installations [°C]

- A file named `Results_FLS_I_"file_name.xlsx"` is also generated. I contains all the outputs.

# Arguments:

1. `file_name`: name of the excel file containing the input for the function. The file must follow the template in "Template.xlsx" and be in the folder `Projects`.

"""
function FLS_I(file_name::String)
## Load the excel file with input data. The file must be in the folder Projects. Julia must be activated in the folder GSHPsDesigner
    access_file = XLSX.readxlsx("Projects/"*file_name)

## Check that the user specified the number of years to simulate
    steps = access_file["Other input"]["E2"]
    if typeof(steps) <: Missing
        println("\nYou have not specified how many time steps you want to simulate. Please, fill cell 'E2' in the 'Other input' sheet of the excel file \n")
        return
    end
    if typeof(access_file["Existing installations"]["H2"]) <: Missing
        println("\nYou have not specified the number of existing borehole. Please, fill cell 'H2' in the 'Existing installations' sheet of the excel file \n")
        return
    end
    if typeof(access_file["Future installations"]["H2"]) <: Missing
        println("\nYou have not specified the number of future borehole. Please, fill cell 'H2' in the 'Future installations' sheet of the excel file \n")
        return
    end

## Extrapolate the ground and borehole field_characteristics
    UndisturbedT = access_file["Other input"]["A2"]
    conductivity = access_file["Other input"]["A6"]
    density = access_file["Other input"]["A9"]
    specificheat = access_file["Other input"]["A12"]
    boreholeradius = access_file["Other input"]["A15"]

## Extrapolate the matrix "field_characteristics", fill the missing values and convert it to the right format
    n_ex = access_file["Existing installations"]["H2"]
    interval = string("A2:F", n_ex + 1)
    field_characteristics = access_file["Existing installations"][interval]
    for ii = 1 : n_ex
        if typeof(field_characteristics[ii,4]) <: Missing
            field_characteristics[ii,4] = 6.; # 6 m is used as a default buried depth
        end
        if typeof(field_characteristics[ii,6]) <: Missing
            field_characteristics[ii,6] = field_characteristics[ii,5] * 15 * 8760/1000 ; # 15 W/m is used as a default linear heat extraction load
        end
    end
    field_characteristics = convert(Array{AbstractFloat,2}, field_characteristics)

## Extrapolate the matrix "new_borehole_characteristics", fill the missing values and convert it to the right format
    new_borehole_characteristics = access_file["New borehole"]["A2:F2"]
        if typeof(new_borehole_characteristics[4]) <: Missing
            new_borehole_characteristics[4] = 6.; # 6 m is used as a default buried depth
        end
        if typeof(new_borehole_characteristics[6]) <: Missing
            new_borehole_characteristics[6] = new_borehole_characteristics[5] * 15 * 8760/1000; # 15 W/m is used as a default linear heat extraction load
        end
        new_borehole_characteristics = convert(Array{AbstractFloat,2}, new_borehole_characteristics)

## Extrapolate the matrix "new_field_characteristics" (if existing), fill the missing values and convert it to the right format
    n_f = access_file["Future installations"]["H2"]
    if n_f > 0
        interval = string("A2:F", n_f + 1)
        new_field_characteristics = access_file["Future installations"][interval]
        for ii = 1 : n_f
            new_field_characteristics[ii,1] = new_borehole_characteristics[1]; # 6 m is used as a default buried depth
            if typeof(new_field_characteristics[ii,4]) <: Missing
                new_field_characteristics[ii,4] = 6.; # 6 m is used as a default buried depth
            end
            if typeof(new_field_characteristics[ii,6]) <: Missing
                new_field_characteristics[ii,6] = new_field_characteristics[ii,5] * 15 * 8760/1000; # 15 W/m is used as a default linear heat extraction load
            end
        end
        new_field_characteristics = convert(Array{AbstractFloat,2}, new_field_characteristics)
## Obtain the results if future installations are considered
        results = FLS_I(field_characteristics, new_borehole_characteristics, new_field_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius)
    else
## Obtain the results if future installations are NOT considered
        results = FLS_I(field_characteristics, new_borehole_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius)
    end

## Export the results on a new excel file
    XLSX.openxlsx("Projects/Results_FLS_I_"*file_name, mode="w") do xf
    XLSX.rename!(xf[1], "With new installation")
    for sheetname in ["Without new installation", "New installation if undisturbed", "With future installations"]
      XLSX.addsheet!(xf, sheetname)
    end
    for ii = 1 : 4
        xf[ii]["A1"] = string("Year"*Char(8594));
        for jj = 1 : steps
            xf[ii][string(Char(Int('A')+jj),1)] = new_borehole_characteristics[1] + jj;
        end
    end

    endcell = string(Char(Int('A'+steps)),n_ex+2)
    xf[1][String("B2:"*endcell)] = round.(results[1], digits =1)

    endcell = string(Char(Int('A'+steps)),n_ex+1)
    xf[2][String("B2:"*endcell)] = round.(results[2], digits =1)

    for jj = 1:n_ex
        xf[1][string("A"*string(jj+1))] = String("BH "*string(jj)*", T[°C]")
        if jj<=n_ex
            xf[2][string("A"*string(jj+1))] = String("BH "*string(jj)*", T[°C]")
        end
        if jj == n_ex
            xf[1][string("A"*string(jj+2))] = "New BH, T[°C]"
        end

    end
    endcell = string(Char(Int('A'+steps)),n_ex+2)
    xf[3][String("B"*string(n_ex+2)*":"*endcell)] = round.(results[3], digits = 1)
    xf[3][string("A"*string(n_ex+2))] = "New BH, T[°C]"

    if n_f > 0
        endcell = string(Char(Int('A'+steps)),n_ex+n_f+2)
        xf[4][String("B2:"*endcell)] = round.(results[4], digits = 1)
        for jj = 1:n_ex+n_f+1
            xf[4][string("A"*string(jj+1))] = String("BH "*string(jj)*", T[°C]")
            if jj == n_ex +1
                xf[4][string("A"*string(jj+1))] = "New BH, T[°C]"
            end
        end
    end
    end
return results
end
