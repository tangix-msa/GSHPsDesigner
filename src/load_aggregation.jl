## Implementation of Claesson and Javed 2012
#  Reference:
#   "Claesson, J, Javed, S, 2011. An analytical method to calculate borehole fluid
#   temperatures for time-scales from minutes to decades. ASHRAE Transactions,
#   vol. 117(2), pp. 279-288."
"""
    κ_CJ(response::Vector{Float64}; n_steps::Int64 = 20 * 8760, P_q::Int64 = 5)

Create the matrix of lumped weighting factors κ for the algorithm of Claesson Javed.

# Output:
 - 'κ': matrix of lumped weighting factor [-]

# Arguments:

1. 'response': vector of DIMENSIONAL thermal response [K]

## Keyword arguments (optional):

2. 'n_steps': number of time steps to simulate [-]
3. 'P_q': number of lumped cells per level [-]
"""
function κ_CJ(response::Vector{Float64}; n_steps::Int64 = 20 * 8760, P_q::Int64 = 5)
    q_max = Int64(ceil(log(2,n_steps/P_q+1)))

    r = fill(0, q_max)
    ν_max = 0;
    for qq = 1 : q_max
        r[qq] = 2^(qq-1)
        ν_max += P_q * r[qq]
    end

    ##  Building ν
    ν = fill(0,q_max, P_q+1)
    ν[1,1]  = 0;
    for qq in 1:q_max
        for pp in 1 : P_q
            ν[qq,pp+1] = ν[qq,1] + r[qq] * pp;
        end
        if qq < q_max
            ν[qq+1,1] =  ν[qq,1] + r[qq] * P_q;
        end
    end
    ##  Building κ
    κ = fill(0.,q_max, P_q);

    for qq in 1:q_max
        for pp in 1: P_q
            if pp == 1 && qq == 1
                κ[qq,pp] = (response[ν[qq,pp+1]]) / response[ν_max];
            else
                κ[qq,pp] = (response[ν[qq,pp+1]] - response[ν[qq,pp]]) / response[ν_max];
            end
        end
    end
    return κ, ν_max
end
"""
    r_ss_CJ(resp_ν_max::Float64, H::Float64)

Calculate the thermal resistance at steady-state.

# Output:

 - 'R_ss': thermal resistance at steady-state [K/W]

# Arguments:

1. 'resp_ν_max': term of the vector of DIMENSIONAL thermal response at time step ν_max [K]
2. 'H': load used for the calculation of the thermal response [W]
"""
function r_ss_CJ(resp_ν_max::Float64, H::Float64)
    R_ss = resp_ν_max / H;
end
"""
    aggregated_load_CJ(q::Vector{Float64}; n_steps::Int64 = 20 * 8760, P_q::Int64 = 5)

Update the aggregated loads for the algorithm of Claesson Javed.
Obs! The formulas are slighlty different from eq. 17 in the paper
because the indeces in eq. 17 seems wrong.

# Output:

 - 'q_aggregated_now': updated matrix of aggregated loads [W]

 # Arguments:

 1. 'q_new': new real load [W]
 2. 'q_aggregated_past': matrix of aggregated loads at the previous time step [W]
"""
function aggregated_load_CJ(q_new::Float64, q_aggregated_past::Matrix{Float64})
    q_max = size(q_aggregated_past)[1];
    P_q = size(q_aggregated_past)[2] - 1;
    q_aggregated_now = fill(0., q_max, P_q + 1);
    q_aggregated_now[1,2] = q_new;
    for qq = 1 : q_max
        r_q = 2^(qq-1);
        if qq == 1
            for pp = 3 : P_q + 1
                q_aggregated_now[qq,pp] = q_aggregated_past[qq,pp] + 1/r_q * (q_aggregated_past[qq,pp-1] - q_aggregated_past[qq,pp])
            end
        else
            for pp = 2 : P_q + 1
                q_aggregated_now[qq,pp] = q_aggregated_past[qq,pp] + 1/r_q * (q_aggregated_past[qq,pp-1] - q_aggregated_past[qq,pp])
            end
        end
    end
    for qq = 2 : q_max
        q_aggregated_now[qq,1] = q_aggregated_now[qq-1,1+P_q];
    end
    return q_aggregated_now
end
"""
    function aggregated_load_CJ(n_steps::Int64; P_q::Int64 = 5)

Initialize the matrix of aggregated loads
"""
function aggregated_load_CJ(n_steps::Int64; P_q::Int64 = 5)
    q_max = Int64(ceil(log(2,n_steps/P_q + 1)));
    aggregated_loads = fill(0.,q_max,P_q+1);

    return aggregated_loads
end

"""
    calculate_T_CJ(R_ss::Float64, q_aggregated::Matrix{Float64}, κ::Matrix{Float64})

Calculate the actual* temperature.
* approximated because of the load aggregation

# Output:

 - 'Tf': temperature [K]

# Arguments:

1. 'R_ss': thermal resistance at steady-state [K/W]
2. 'q_aggregated': matrix of aggregated loads [W]
3. 'κ': matrix of lumped weighting factor [-]
"""
function calculate_T_CJ(R_ss::Float64, q_aggregated::Matrix{Float64}, κ::Matrix{Float64})
    Tf = R_ss * sum(sum(q_aggregated[:,2:end] .* κ));
    return Tf
end
