"""
    FLS_IV(field_characteristics::Array{T,2}, tmax::Int; k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}

Evaluate the **borehole wall temperatures** in a neighbourhood with independent ground source heat pump installations.

# Outputs

1.  `ordered_field_characteristics`: list of the boreholes (or better their characteristics), ordered for year of installation.
2.  `temperatures`: evolution of the temperature change on each borehole [K]. The order of the boreholes is the same as in result 1.
3.  `solution`: non-dimensional temperature evolution (to be divided by 2*pi*k to obtain the dimensional temperature evolution) and load profile of the boreholes. The order of the boreholes is the same as in result 1.

# Arguments

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `tmax`: final timestep at which to calculate the temperature [year]

## Keyword arguments
- `k = 3.1`:  ground thermal conductivity [W/mK]
- `ro = 2300`: ground density [kg/m3]
- `cp = 870`: ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`: borehole radius [m]
- `m::Int=12`: number of segments per borehole [-]

Implementation of the FLS model with BC IV: uniform temperature along the boreholes.
"""
function FLS_IV(field_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}
    if size(field_characteristics)[2] != 6
        println("The matrix field_characteristics should have 6 coloumns");
    elseif tmax < 0
        println("tmax must be a positive number");
    else
    αg = k/ ro/ cp;
    t = collect(1:tmax).* tstep; # [s] time at which the solutions are evaluated
    first_year = minimum(field_characteristics[:,1]);

    complete_field_characteristics = field_characteristics
    n_total_boreholes = length(complete_field_characteristics[:,1]);

    n_segments = n_total_boreholes * m;
    unique_years = Int64.(sort(unique(complete_field_characteristics[:,1])));
    n_unique_years= length(unique_years);
    complete_field_characteristics = hcat(complete_field_characteristics, collect(1:n_total_boreholes));
    ordered_field_characteristics = sortslices(complete_field_characteristics, dims = 1)
    distances = fill(0.,n_total_boreholes,n_total_boreholes);
    for ii = 1: n_total_boreholes
        for jj = 1:n_total_boreholes
            distances[ii,jj] = euclidean(ordered_field_characteristics[ii,2:3],ordered_field_characteristics[jj,2:3]);
        end
    end
    z_matrix = fill(0., n_total_boreholes, m)
    for ii = 1 : n_total_boreholes
        z_matrix[ii,:] = collect(ordered_field_characteristics[ii,4] : ordered_field_characteristics[ii,5]/m : ordered_field_characteristics[ii,4]+(m-1)*ordered_field_characteristics[ii,5]/m*100.1/100)  #*100.1/100 is added for computational reason
    end
    G3 = fill(Point3{Float64}(0.,0.,0.), n_segments);
    for ii = 1 : n_total_boreholes
        for jj = 1:m
            G3[(ii-1)*m+jj] = Point3{Float64}(ordered_field_characteristics[ii,2],ordered_field_characteristics[ii,3],z_matrix[ii,jj])
        end
    end
    h = fill(0., n_segments, n_segments, tmax);
    for ii = 1 : n_segments
        ii_subindex = Int64(ceil(ii/m));
        for jj = 1 : n_segments
            jj_subindex = Int64(ceil(jj/m));
            h[ii,jj,:] = fls(t, αg, max(distances[ii_subindex,jj_subindex],rb), G3[jj][3], G3[ii][3], ordered_field_characteristics[Int64(ceil(jj/m)),5]/m, ordered_field_characteristics[Int64(ceil(ii/m)),5]/m);
        end
    end
    power_borehole = fill(0.,n_total_boreholes, tmax);
    for ii = 1 : tmax
        for jj = 1 : n_total_boreholes
            if ii > ordered_field_characteristics[jj,1] - first_year
                power_borehole[jj,ii] = ordered_field_characteristics[jj,6] *1000/8760;
            else
                power_borehole[jj,ii] = 0.;
            end
        end
    end
    solution = fill(0., n_total_boreholes + n_segments, tmax);
    for ii = 1 : n_unique_years
       tmax_to_consider = tmax
       boreholes_to_consider = n_total_boreholes
    # Building matrix A_____________________________________________________________
        segments_to_consider = boreholes_to_consider * m;
        A1 = fill(0., boreholes_to_consider, boreholes_to_consider);
        A2 = fill(0.,boreholes_to_consider, segments_to_consider);
        A3 = fill(0., segments_to_consider, boreholes_to_consider);
        for zz = 1 : boreholes_to_consider
            for jj = (zz-1) * m + 1: (zz) * m
                A2[zz,jj] = ordered_field_characteristics[zz,5]/m;        # construction matrix A2
                A3[jj,zz] = -1;       # construction matrix A3
            end
        end
        A4 = h[1 : segments_to_consider, 1 : segments_to_consider , 1]
        AA = vcat(hcat(A1, A2),hcat(A3,A4));
        A_inv= inv(AA);
    # end (Building matrix A)_____________________________________________________
    # Initialization of the solution vector_________________________________________
        x = fill(0., boreholes_to_consider + segments_to_consider, tmax_to_consider);
    # end (Initialization of the solution vector)___________________________________
    # Building b0_______________________________________________________________
        b01 = fill(0.,boreholes_to_consider, tmax_to_consider);
        b01[:,1] = power_borehole[1:boreholes_to_consider,1];
        for TT = 2: tmax_to_consider
            b01[:,TT] = power_borehole[1:boreholes_to_consider,TT] .- power_borehole[1:boreholes_to_consider,TT-1];
        end
            b0 = vcat(b01[:,1], fill(0., segments_to_consider));
    # end (Building b0)_________________________________________________________
    # Solution at the first time step_______________________________________________
        x[:,1] = A_inv * b0;
        solution[1 : boreholes_to_consider, 1] = x[1: boreholes_to_consider, 1];
        solution[n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, 1] = x[boreholes_to_consider + 1 : end, 1];
    # end (Solution at the first time step)
        if tmax_to_consider > 1
            for TT = 2 : tmax_to_consider
                convolution = fill(0., segments_to_consider, 1);
                for kk = 1 : TT-1
                    convolution = convolution .+ (h[1:segments_to_consider,1:segments_to_consider,TT-(kk-1)]  * solution[ n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, kk])
                end
                b = vcat(b01[:,TT], - convolution);
                x[:,TT] = A_inv * b;
                solution[1 : boreholes_to_consider, TT ] = x[1: boreholes_to_consider, TT];
                solution[n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, TT] = x[boreholes_to_consider + 1 : end, TT]
            end
        end
    end
    temperatures = T_ground .- solution[1:n_total_boreholes,:] ./ (2*pi*k);
end
return ordered_field_characteristics, temperatures, solution
end # FLS_IV(field_characteristics::Array{T,2}, tmax::Int; k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}
"""
    FLS_IV(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int; k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}

Evaluate the **borehole wall temperatures** in a neighbourhood with independent ground source heat pump installations.

# Outputs

1.  `ordered_field_characteristics`: list of the boreholes (or better their characteristics), ordered for year of installation.
2.  `temperatures`: evolution of the temperature change on each borehole [K]. The order of the boreholes is the same as in result 1.
3.  `solution`: non-dimensional temperature evolution (to be divided by 2*pi*k to obtain the dimensional temperature evolution) and load profile of the boreholes. The order of the boreholes is the same as in result 1.

# Arguments

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `tmax`: final timestep at which to calculate the temperature [year]

## Keyword arguments
- `k = 3.1`:  ground thermal conductivity [W/mK]
- `ro = 2300`: ground density [kg/m3]
- `cp = 870`: ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`: borehole radius [m]
- `m::Int=12`: number of segments per borehole [-]

Implementation of the FLS model with BC IV: uniform temperature along the boreholes.
"""
function FLS_IV(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}
    if size(field_characteristics)[2] != 6 || size(new_borehole_characteristics)[2] != 6
        println("The matrices field_characteristics and new_borehole_characteristics should have 6 coloumns");
    elseif tmax < 0
        println("tmax must be a positive number");
    else
    αg = k/ ro/ cp;
    first_year = minimum(field_characteristics[:,1]);
    t = collect(1:tmax+Int(new_borehole_characteristics[1] - first_year)).* tstep; # [s] time at which the solutions are evaluated

    n_existing_boreholes = length(field_characteristics[:,1]);
    complete_field_characteristics = vcat(field_characteristics, new_borehole_characteristics);
    n_total_boreholes = length(complete_field_characteristics[:,1]);

    n_segments = n_total_boreholes * m;
    unique_years = Int64.(sort(unique(complete_field_characteristics[:,1])));
    n_unique_years= length(unique_years);
    complete_field_characteristics = hcat(complete_field_characteristics, collect(1:n_total_boreholes));
    ordered_field_characteristics = sortslices(complete_field_characteristics, dims = 1)
    distances = fill(0.,n_total_boreholes,n_total_boreholes);
    for ii = 1: n_total_boreholes
        for jj = 1:n_total_boreholes
            distances[ii,jj] = euclidean(ordered_field_characteristics[ii,2:3],ordered_field_characteristics[jj,2:3]);
        end
    end
    z_matrix = fill(0., n_total_boreholes, m)
    for ii = 1 : n_total_boreholes
        z_matrix[ii,:] = collect(ordered_field_characteristics[ii,4] : ordered_field_characteristics[ii,5]/m : ordered_field_characteristics[ii,4]+(m-1)*ordered_field_characteristics[ii,5]/m*100.1/100)  #*100.1/100 is added for computational reason
    end
    G3 = fill(Point3{Float64}(0.,0.,0.), n_segments);
    for ii = 1 : n_total_boreholes
        for jj = 1:m
            G3[(ii-1)*m+jj] = Point3{Float64}(ordered_field_characteristics[ii,2],ordered_field_characteristics[ii,3],z_matrix[ii,jj])
        end
    end
    h = fill(0., n_segments, n_segments, tmax+Int(new_borehole_characteristics[1] - first_year));
    for ii = 1 : n_segments
        ii_subindex = Int64(ceil(ii/m));
        for jj = 1 : n_segments
            jj_subindex = Int64(ceil(jj/m));
            h[ii,jj,:] = fls(t, αg, max(distances[ii_subindex,jj_subindex],rb), G3[jj][3], G3[ii][3], ordered_field_characteristics[Int64(ceil(jj/m)),5]/m, ordered_field_characteristics[Int64(ceil(ii/m)),5]/m);
        end
    end
    power_borehole = fill(0.,n_total_boreholes, tmax+ Int(new_borehole_characteristics[1] - first_year));
    for ii = 1 : tmax + Int(new_borehole_characteristics[1] - first_year)
        for jj = 1 : n_total_boreholes
            if ii > ordered_field_characteristics[jj,1] - first_year
                power_borehole[jj,ii] = ordered_field_characteristics[jj,6] *1000/8760;
            else
                power_borehole[jj,ii] = 0.;
            end
        end
    end
    solution = fill(0., n_total_boreholes + n_segments, tmax + Int(new_borehole_characteristics[1] - first_year));

    tmax_to_consider = tmax+Int(new_borehole_characteristics[1] - first_year)
    boreholes_to_consider = n_total_boreholes

    # Building matrix A_____________________________________________________________
    segments_to_consider = boreholes_to_consider * m;
    A1 = fill(0., boreholes_to_consider, boreholes_to_consider);
    A2 = fill(0.,boreholes_to_consider, segments_to_consider);
    A3 = fill(0., segments_to_consider, boreholes_to_consider);
    for zz = 1 : boreholes_to_consider
        for jj = (zz-1) * m + 1: (zz) * m
            A2[zz,jj] = ordered_field_characteristics[zz,5]/m;        # construction matrix A2
            A3[jj,zz] = -1;       # construction matrix A3
        end
    end
    A4 = h[1 : segments_to_consider, 1 : segments_to_consider , 1]
    AA = vcat(hcat(A1, A2),hcat(A3,A4));
    A_inv= inv(AA);
# end (Building matrix A)_____________________________________________________
# Initialization of the solution vector_________________________________________
    x = fill(0., boreholes_to_consider + segments_to_consider, tmax_to_consider);
# end (Initialization of the solution vector)___________________________________
# Building b0_______________________________________________________________
    b01 = fill(0.,boreholes_to_consider, tmax_to_consider);
    b01[:,1] = power_borehole[1:boreholes_to_consider,1];
    for TT = 2: tmax_to_consider
        b01[:,TT] = power_borehole[1:boreholes_to_consider,TT] .- power_borehole[1:boreholes_to_consider,TT-1];
    end
    b0 = vcat(b01[:,1], fill(0., segments_to_consider));
# end (Building b0)_________________________________________________________
# Solution at the first time step_______________________________________________
    x[:,1] = A_inv * b0;
    solution[1 : boreholes_to_consider, 1] = x[1: boreholes_to_consider, 1];
    solution[n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, 1] = x[boreholes_to_consider + 1 : end, 1];
# end (Solution at the first time step)

    if tmax_to_consider > 1
        for TT = 2 : tmax_to_consider
            convolution = fill(0., segments_to_consider, 1);
            for kk = 1 : TT-1
                convolution = convolution .+ (h[1:segments_to_consider,1:segments_to_consider,TT-(kk-1)]  * solution[ n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, kk])
            end
            b = vcat(b01[:,TT], - convolution);
            x[:,TT] = A_inv * b;
            solution[1 : boreholes_to_consider, TT ] = x[1: boreholes_to_consider, TT];
            solution[n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, TT] = x[boreholes_to_consider + 1 : end, TT]
        end
    end

    temperatures = T_ground .- solution[1:n_total_boreholes,:] ./ (2*pi*k);
end
return ordered_field_characteristics, temperatures, solution
end # FLS_IV(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int; k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}
"""
    FLS_IV(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2},  tmax::Int; k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}

Evaluate the **borehole wall temperature changes** in a neighbourhood with independent ground source heat pump installations.

# Outputs

1.  `ordered_field_characteristics`: list of the boreholes (or better their characteristics), ordered for year of installation.
2.  `temperatures`: evolution of the temperature change on each borehole [K]. The order of the boreholes is the same as in result 1.
3.  `solution`: non-dimensional temperature evolution (to be divided by 2*pi*k to obtain the dimensional temperature evolution) and load profile of the boreholes. The order of the boreholes is the same as in result 1.

# Arguments

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `new_field_characteristics`: matrix containing the following characteristics for the possible future boreholes: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
4. `tmax`: final timestep at which to calculate the temperature [year]

## Keyword arguments
- `k = 3.1`:  ground thermal conductivity [W/mK]
- `ro = 2300`: ground density [kg/m3]
- `cp = 870`: ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`: borehole radius [m]
- `m::Int=12`: number of segments per borehole [-]

Implementation of the FLS model with BC IV: uniform temperature along the boreholes.
"""
function FLS_IV(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2},  tmax::Int; T_ground = 8., tstep::T=8760*3600., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}
    if size(field_characteristics)[2] != 6 || size(new_borehole_characteristics)[2] != 6 || size(new_field_characteristics)[2] != 6
        println("The matrices field_characteristics and new_borehole_characteristics should have 6 coloumns");
    elseif tmax < 0
        println("tmax must be a positive number");
    else
    αg = k/ ro/ cp;

    first_year = minimum(field_characteristics[:,1]);
    t = collect(1:tmax+Int(new_borehole_characteristics[1] - first_year)).* tstep; # [s] time at which the solutions are evaluated

    n_existing_boreholes = length(field_characteristics[:,1]);
    for ii  = 1 : size(new_field_characteristics)[1]
        new_field_characteristics[ii,1] = new_borehole_characteristics[1]
    end
    complete_field_characteristics = vcat(field_characteristics, new_borehole_characteristics, new_field_characteristics);
    n_total_boreholes = length(complete_field_characteristics[:,1]);

    n_segments = n_total_boreholes * m;
    unique_years = Int64.(sort(unique(complete_field_characteristics[:,1])));
    n_unique_years= length(unique_years);
    complete_field_characteristics = hcat(complete_field_characteristics, collect(1:n_total_boreholes));
    ordered_field_characteristics = sortslices(complete_field_characteristics, dims = 1)
    distances = fill(0.,n_total_boreholes,n_total_boreholes);
    for ii = 1: n_total_boreholes
        for jj = 1:n_total_boreholes
            distances[ii,jj] = euclidean(ordered_field_characteristics[ii,2:3],ordered_field_characteristics[jj,2:3]);
        end
    end
    z_matrix = fill(0., n_total_boreholes, m)
    for ii = 1 : n_total_boreholes
        z_matrix[ii,:] = collect(ordered_field_characteristics[ii,4] : ordered_field_characteristics[ii,5]/m : ordered_field_characteristics[ii,4]+(m-1)*ordered_field_characteristics[ii,5]/m*100.1/100)  #*100.1/100 is added for computational reason
    end
    G3 = fill(Point3{Float64}(0.,0.,0.), n_segments);
    for ii = 1 : n_total_boreholes
        for jj = 1:m
            G3[(ii-1)*m+jj] = Point3{Float64}(ordered_field_characteristics[ii,2],ordered_field_characteristics[ii,3],z_matrix[ii,jj])
        end
    end
    h = fill(0., n_segments, n_segments, tmax+Int(new_borehole_characteristics[1] - first_year));
    for ii = 1 : n_segments
        ii_subindex = Int64(ceil(ii/m));
        for jj = 1 : n_segments
            jj_subindex = Int64(ceil(jj/m));
            h[ii,jj,:] = fls(t, αg, max(distances[ii_subindex,jj_subindex],rb), G3[jj][3], G3[ii][3], ordered_field_characteristics[Int64(ceil(jj/m)),5]/m, ordered_field_characteristics[Int64(ceil(ii/m)),5]/m);
        end
    end
    power_borehole = fill(0.,n_total_boreholes, tmax+Int(new_borehole_characteristics[1]));
    for ii = 1:tmax+Int(new_borehole_characteristics[1] -first_year)
        for jj = 1 : n_total_boreholes
            if ii > ordered_field_characteristics[jj,1] - first_year
                power_borehole[jj,ii] = ordered_field_characteristics[jj,6] *1000/8760;
            else
                power_borehole[jj,ii] = 0.;
            end
        end
    end
    solution = fill(0., n_total_boreholes + n_segments, tmax+Int(new_borehole_characteristics[1] -first_year));

           tmax_to_consider = tmax+Int(new_borehole_characteristics[1] -first_year)
           boreholes_to_consider = n_total_boreholes

    # Building matrix A_____________________________________________________________
        segments_to_consider = boreholes_to_consider * m;
        A1 = fill(0., boreholes_to_consider, boreholes_to_consider);
        A2 = fill(0.,boreholes_to_consider, segments_to_consider);
        A3 = fill(0., segments_to_consider, boreholes_to_consider);
        for zz = 1 : boreholes_to_consider
            for jj = (zz-1) * m + 1: (zz) * m
                A2[zz,jj] = ordered_field_characteristics[zz,5]/m;        # construction matrix A2
                A3[jj,zz] = -1;       # construction matrix A3
            end
        end
        A4 = h[1 : segments_to_consider, 1 : segments_to_consider , 1]
        AA = vcat(hcat(A1, A2),hcat(A3,A4));
        A_inv= inv(AA);
    # end (Building matrix A)_____________________________________________________
    # Initialization of the solution vector_________________________________________
        x = fill(0., boreholes_to_consider + segments_to_consider, tmax_to_consider);
    # end (Initialization of the solution vector)___________________________________
    # Building b0_______________________________________________________________
        b01 = fill(0.,boreholes_to_consider, tmax_to_consider);
        b01[:,1] = power_borehole[1:boreholes_to_consider,1];
        for TT = 2: tmax_to_consider
            b01[:,TT] = power_borehole[1:boreholes_to_consider,TT] .- power_borehole[1:boreholes_to_consider,TT-1];
        end

            b0 = vcat(b01[:,1], fill(0., segments_to_consider));
    # end (Building b0)_________________________________________________________
    # Solution at the first time step_______________________________________________
            x[:,1] = A_inv * b0;
            solution[1 : boreholes_to_consider, 1] = x[1: boreholes_to_consider, 1];
            solution[n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, 1] = x[boreholes_to_consider + 1 : end, 1];
    # end (Solution at the first time step)

    if tmax_to_consider > 1
        for TT = 2 : tmax_to_consider
            convolution = fill(0., segments_to_consider, 1);
            for kk = 1 : TT-1
                convolution = convolution .+ (h[1:segments_to_consider,1:segments_to_consider,TT-(kk-1)]  * solution[ n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, kk])
            end
            b = vcat(b01[:,TT], - convolution);
            x[:,TT] = A_inv * b;
            solution[1 : boreholes_to_consider, TT ] = x[1: boreholes_to_consider, TT];
            solution[n_total_boreholes + 1 : n_total_boreholes + segments_to_consider, TT] = x[boreholes_to_consider + 1 : end, TT]
        end
    end

    temperatures = T_ground .- solution[1:n_total_boreholes,:] ./ (2*pi*k);
end
return ordered_field_characteristics, temperatures, solution
end # FLS_IV(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2},  tmax::Int; k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, m::Int=12) where {T<:AbstractFloat}
"""
    FLS_IV(file_name::String)

Evaluate the **borehole walls temperatures** in a neighbourhood with independent ground source heat pump installations.
Implementation of the FLS model with BC IV: uniform temperature along the boreholes.

# Output:

1. `temperature_with_new_installation`: borehole temperature evolution of all the installations taking into account the new installation (The first rows contain the results for the existing installations in the same order as the input given to the function. The last row contains the results for the new installation) [°C]
2. `temperature_without_new_installation`: borehole temperature evolution without the new installation [°C]
3. `temperature_new_borehole_if_undisturbed`: borehole temperature evolution on the new installation if isolated [°C]
4. `temperature_with_all_installations`: borehole temperature evolution taking into account also the possible future installations [°C]

- A file named `Results_FLS_IV_"file_name.xlsx"` is also generated. I contains all the outputs.

# Arguments:

1. `file_name`: name of the excel file containing the input for the function. The file must follow the template in "Template.xlsx" and be in the folder `Projects`.

## Keyword arguments
- `m::Int=12`: number of segments per borehole [-]
"""
function FLS_IV(file_name::String; segments::Int=12)
## Load the excel file with input data. The file must be in the folder Projects. Julia must be activated in the folder GSHPsDesigner
    access_file = XLSX.readxlsx("Projects/"*file_name)

## Check that the user specified the number of time steps to simulate
    steps = access_file["Other input"]["E2"]
    if typeof(steps) <: Missing
        println("\nYou have not specified how many time steps you want to simulate. Please, fill cell 'E2' in the 'Other input' sheet of the excel file \n")
        return
    end

## Extrapolate the ground and borehole field_characteristics
        UndisturbedT = access_file["Other input"]["A2"]
        conductivity = access_file["Other input"]["A6"]
        density = access_file["Other input"]["A9"]
        specificheat = access_file["Other input"]["A12"]
        boreholeradius = access_file["Other input"]["A15"]

## Extrapolate the matrix "field_characteristics", fill the missing values and convert it to the right format
    n_ex = access_file["Existing installations"]["H2"]
    interval = string("A2:F",n_ex+1)
    field_characteristics = access_file["Existing installations"][interval]
    for ii = 1 : n_ex
        if typeof(field_characteristics[ii,4]) <: Missing
            field_characteristics[ii,4] = 6.; # 6 m is used as a default buried depth
        end
        if typeof(field_characteristics[ii,6]) <: Missing
            field_characteristics[ii,6] = field_characteristics[ii,5] * 15 * 8760/1000 ; # 15 W/m is used as a default linear heat extraction load
        end
    end
    field_characteristics = convert(Array{AbstractFloat,2}, field_characteristics)

## Extrapolate the matrix "new_borehole_characteristics", fill the missing values and convert it to the right format
    new_borehole_characteristics = access_file["New borehole"]["A2:F2"]
        if typeof(new_borehole_characteristics[4]) <: Missing
            new_borehole_characteristics[4] = 6.; # 6 m is used as a default buried depth
        end
        if typeof(new_borehole_characteristics[6]) <: Missing
            new_borehole_characteristics[6] = new_borehole_characteristics[5] * 15 * 8760/1000; # 15 W/m is used as a default linear heat extraction load
        end
        new_borehole_characteristics = convert(Array{AbstractFloat,2}, new_borehole_characteristics)

## Extrapolate the matrix "new_field_characteristics" (if existing), fill the missing values and convert it to the right format
    n_f = access_file["Future installations"]["H2"]

    if n_f > 0
## Obtain the results if future installations are considered
        interval = string("A2:F", n_f + 1)
        new_field_characteristics = access_file["Future installations"][interval]
        for ii = 1 : n_f
            new_field_characteristics[ii,1] = new_borehole_characteristics[1];
            if typeof(new_field_characteristics[ii,4]) <: Missing
                new_field_characteristics[ii,4] = 6.; # 6 m is used as a default buried depth
            end
            if typeof(new_field_characteristics[ii,6]) <: Missing
                new_field_characteristics[ii,6] = new_field_characteristics[ii,5] * 15 * 8760/1000 ; # 15 W/m is used as a default linear heat extraction load
            end
        end
        new_field_characteristics = convert(Array{AbstractFloat,2}, new_field_characteristics)

        resultsfuture = FLS_IV(field_characteristics, new_borehole_characteristics, new_field_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)
        order = resultsfuture[1][:,end];
        orderedtemperaturesfuture = sortslices(hcat(order, resultsfuture[2]), dims=1);

        resultsfield = FLS_IV(field_characteristics, new_borehole_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)
        order = resultsfield[1][:,end];
        orderedtemperaturesfield = sortslices(hcat(order, resultsfield[2]), dims=1);

        stepsold = Int(steps + new_borehole_characteristics[1] - minimum(field_characteristics[:,1]));
        resultsold = FLS_IV(field_characteristics,  stepsold, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)
        order = resultsold[1][:,end];
        orderedtemperaturesold = sortslices(hcat(order, resultsold[2]), dims=1);

        resultsnew = FLS_IV( new_borehole_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)[2]

    else
## Obtain the results if future installations are NOT considered
        resultsfield = FLS_IV(field_characteristics, new_borehole_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)
        order = resultsfield[1][:,end];
        orderedtemperaturesfield = sortslices(hcat(order, resultsfield[2]), dims=1);

        stepsold = Int(steps + new_borehole_characteristics[1] - minimum(field_characteristics[:,1]));
        resultsold = FLS_IV(field_characteristics,  stepsold, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)
        order = resultsold[1][:,end];
        orderedtemperaturesold = sortslices(hcat(order, resultsold[2]), dims=1);

        resultsnew = FLS_IV( new_borehole_characteristics, steps, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius, m = segments)[2]

    end

## Export the results on a new excel file
    XLSX.openxlsx("Projects/Results_FLS_IV_"*file_name, mode="w") do xf
    XLSX.rename!(xf[1], "With new installation")

    for sheetname in ["Without new installation", "New installation if undisturbed", "With future installations"]
      XLSX.addsheet!(xf, sheetname)
    end

    for ii = 1 : 4
        xf[ii]["A1"] = string("Year"*Char(8594));
        for jj = 1 : steps
            xf[ii][string(Char(Int('A')+jj),1)] = new_borehole_characteristics[1] + jj;
        end
    end

    endcell = string(Char(Int('A'+steps)),n_ex+2)
    xf[1][String("B2:"*endcell)] = round.(orderedtemperaturesfield[:,end-steps+1:end], digits =1)

    endcell = string(Char(Int('A'+steps)),n_ex+1)
    xf[2][String("B2:"*endcell)] = round.(orderedtemperaturesold[:, end-steps+1:end], digits =1)

    for jj = 1 : n_ex
        xf[1][string("A"*string(jj+1))] = String("BH "*string(jj)*", T[°C]")
        if jj == n_ex
            xf[1][string("A"*string(jj+2))] = "New BH, T[°C]"
        end
        if jj <= n_ex
            xf[2][string("A"*string(jj+1))] = String("BH "*string(jj)*", T[°C]")
        end
    end
    endcell = string(Char(Int('A'+steps)),n_ex+2)
    xf[3][String("B"*string(n_ex+2)*":"*endcell)] = round.(resultsnew, digits = 1)
    xf[3][string("A"*string(n_ex+2))] = "New BH, T[°C]"

    if n_f > 0
        endcell = string(Char(Int('A'+steps)),n_ex+n_f+2)
        xf[4][String("B2:"*endcell)] = round.(orderedtemperaturesfuture[:,end-steps+1:end], digits = 1)
        for jj = 1:n_ex+n_f+1
            xf[4][string("A"*string(jj+1))] = String("BH "*string(jj)*", T[°C]")
            if jj == n_ex+1
                xf[4][string("A"*string(jj+1))] = "New BH, T[°C]"
            end
        end
    return orderedtemperaturesfield[:,end-steps+1:end], orderedtemperaturesold[:, end-steps+1:end], resultsnew, orderedtemperaturesfuture[:,end-steps+1:end]
    else
    return orderedtemperaturesfield[:,end-steps+1:end], orderedtemperaturesold[:, end-steps+1:end], resultsnew

    end
    end

end
