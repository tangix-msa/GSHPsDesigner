"""
    groundPVheatpump(n_years::Int, field_characteristics::Array{T,2}, H::T, borehole_extra_info::Array{T,1}, Esolar_1::Array{T,1}, ampl_factor::T; T_DHW = 50 + 273.15, DHW_filename = joinpath(pwd(), "data/DHW_load.jld"), load_filename= joinpath(pwd(),"data/Villa00.txt"), T0 = 8., heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv"), T_threshold = -8., nominal_capacity = 7.0, nominal_brine_flowrate_eva = 0.40, nominal_water_flowrate_cond = 0.16, Brine = "INCOMP::MEA-28%", Rb::T = 0.07, rb::T = 0.0575, D = 6., k = 3.1, ro = 2300., c_p = 870.) where {T <:AbstractFloat}


Simulate the operation of a ground-source heat pump for space heating and/or domestic hot water during `n_years` years taking into account the presence of neighbouring installations and
the recharging of the borehole heat exchagners

# Output:

1. `EcompressorSH` electricity used by the compressor in space heating mode [kWh]
2. `EauxSH` energy used by the auxiliary in space heating mode [kWh]
3. `QloadSH` space heating demand [kWh]
4. `EcompressorDHW` electricity used by the compressor in space heating mode [Wh]
5. `EauxDHW` electricity used by the auxiliary in domestic hot water mode [Wh]
6. `QloadDHW` hot water demand [kWh]
7. `Esolar_to_HP` solar electricity that covers the heating demand [kWh]
8. `Esolar_to_ground` solar electricity that recharges the ground [kWh]
9. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
10. `ontimeDHW` share of the hour during which the HP operates in domestic hot water mode [-]
11. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
12. `auxonflagDHW` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in domestic hot water mode [-]
13. `(EgroundSH .+ EgroundDHW)` heat absorbed from the ground [kWh]
14. `T_bh` borehole wall temperature [K]
15. `Teva_in_hp` return temperature from the ground heat exchnager [K]

# Arguments:

1. `n_years` number of years to simulate [-]
2. `field_characteristics` matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `H` borehole length of the borehole of interest [m]
4. `borehole_extra_info` Vector (Array{T,1}!!!) containing [year of installation; x position; y position] of the borehole of interest;
5. `Esolar_1` vector containing the PV overproduction (after appliances) [kWh]
6. `ampl_factor` efficiency at which the solar electricity will be converted into heat for ground recharging

## Keyword arguments:
- `T_DHW` temperature of the return temperature from the domestic hot water production distribution system [K]
- `DHW_filename` absolute path to the file containing the domestic hot water load with hourly resolution for 1 year [kWh or Whis indifferent]
- `load_filename` absolute path to the file containing the space heating load with hourly resolution for 1 year [kWh or Wh is indifferent]
- `T0 ` initial ground temperature [°C]
- `heatpump_catalogue` absolute path to the file containing the csv file for the HP regression [kW, l/s, l/s, -]
- `T_threshold` temperature under which the HP turns off [°C]
- `nominal_capacity` nominal heating capacity of the heat pump [kW]
- `nominal_brine_flowrate_eva` nominal brine flow rate in the evaporator [l/s]
- ` nominal_water_flowrate_cond` nominal water flow rate in the condenser [l/s]
- `Brine` name of the brine in the evaporator
- `rb = 7.5e-2`;        # borehole radius [m]
- `D = 6.`;             # borehole buried depth
- `k = 3.1`;            # ground thermal conductivity [W/mK]
- `ro = 2300`;          # ground density [kg/m3]
- `c_p = 870`;          # ground heating capacity [J/(kg K)]
"""
    function groundPVheatpump(n_years::Int, field_characteristics::Array{T,2}, H::T, borehole_extra_info::Array{T,1}, Esolar_1::Array{T,1}, ampl_factor::T; T_DHW = 50 + 273.15, DHW_filename = joinpath(pwd(), "data/DHW_load.jld"), load_filename= joinpath(pwd(),"data/Villa00.txt"), T0 = 8., heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv"), T_threshold = -8., nominal_capacity = 7.0, nominal_brine_flowrate_eva = 0.40, nominal_water_flowrate_cond = 0.16, Brine = "INCOMP::MEA-28%", Rb::T = 0.07, rb::T = 0.0575, D = 6., k = 3.1, ro = 2300., c_p = 870.) where {T <:AbstractFloat}
        # The simulation involves n_years with hourly resolution
            steps_per_year = 8760;                      # numbed of steps per years [-]
            timestep_length = 3600.;                    # length of a time step [s]
            number_steps = n_years * steps_per_year;    # total number of time steps [-]
        #-------------------------------------------------------
            borehole_characteristics = [D; H+D; H * 8760/1000];
            self_response = abs.(FLS_I(borehole_characteristics, number_steps, tstep = timestep_length, T_ground = 0., rb = rb))
        # The borehole of interest is always positioned in x,y coordinates 0, 0
            complete_borehole_characteristics = reshape(vcat(borehole_extra_info, borehole_characteristics),1, 6);
        #--------------------------------------------------------
            responses = FLS_I(field_characteristics, complete_borehole_characteristics, n_years, T_ground = 0, tstep = steps_per_year * timestep_length, k=k, ro=ro, cp=c_p, rb=rb)
            influence = abs.(responses[1][end,:] .- responses[3][end,:])
    # Determine the load of the house and HP connected to the borehole of interest
            annual_load_house = house_heating_demand(H);                # SH + DHW [kWh]
            (profile, TcondinSH) = profile_from_file(load_filename)     # [-, degC]
            rel_capacity = relative_hp_capacity(profile);               # [-]
            hp_cap = hp_capacity(rel_capacity, annual_load_house);      # [kWh]

            capacity_ratio = hp_cap / nominal_capacity;
            brine_flowrate_eva = nominal_brine_flowrate_eva * capacity_ratio;
    # Compute the regression models for the HP
        regressions = generate_regressions(heatpump_catalogue)
        regrHC = regressions[1];
        regrCOP = regressions[2];

        perDHW = 0.14;
        loadDHW = load(DHW_filename)["DHW_load"];
        profileDHW = loadDHW ./ sum(loadDHW)
        loadSH = annual_load_house * (1 - perDHW) .* profile; # [kWH]
        loadDHW = annual_load_house * perDHW .* profileDHW; # [kWH]
    ## Initializing variables
        T_bh = fill(0.0, number_steps);                                                        # Borehole wall temperature [K]
        T_bh[1] = T0 + 273.;                                                                    # Initial borehole wall temperature [K]
        Teva_in_hp = fill(0.0, number_steps);                                                  # Temperature of the fluid circulating in the borehole
        Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

        Tcond_in_SH = repeat(TcondinSH, n_years);                                                     # Return temperature from the distribution system for space heating (water temperature at condenser inlet) [K]
        Tcond_in_DHW = fill(T_DHW, number_steps);                                                   # Return temperature from the distribution system for domestic hot water (water temperature at condenser inlet) [K]
        QloadSH = repeat(loadSH, n_years);                                                            # Heating demand for space heating [Wh]
        QloadDHW = repeat(loadDHW, n_years);                                                          # Heating demand for domestic hot water [Wh]

        Esolar = repeat(Esolar_1, n_years);
        Esolar_to_HP = fill(0.0, number_steps);
        Esolar_to_ground = fill(0.0, number_steps);

        QevaSH = fill(0.0, number_steps);                                                            # HP cooling capacity in space heating mode [W],
        TevaSH = fill(0.0, number_steps);                                                            # Refrigerant evaporation T in space heating mode [K]
        EevaSH = fill(0.0, number_steps);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
        EgroundSH = fill(0.0, number_steps);                                                         # Energy extracted from the BH in space heating mode [Wh]
        QevaDHW = fill(0.0, number_steps);                                                           # HP cooling capacity in domestic hot water mode [W],
        TevaDHW = fill(0.0, number_steps);                                                           # Refrigerant evaporation T in domestic hot water mode [K]
        EevaDHW = fill(0.0, number_steps);                                                           # Energy absorbed in the evaporator in domestic hot water mode [Wh]
        EgroundDHW = fill(0.0, number_steps);                                                        # Energy extracted from the BH in domestic hot water mode [Wh]

        QcondSH = fill(0.0, number_steps);                                                             # HP Heating capacity in space heating mode [W]
        TcondSH = fill(0.0, number_steps);                                                             # Refrigerant evaporation T in space heating mode [K]
        EcondSH = fill(0.0, number_steps);                                                             # Energy realeased in the condenser in space heating mode [Wh]
        QcondDHW = fill(0.0, number_steps);                                                             # HP Heating capacity in domestic hot water mode [W]
        TcondDHW = fill(0.0, number_steps);                                                             # Refrigerant evaporation T in domestic hot water mode [K]
        EcondDHW = fill(0.0, number_steps);                                                             # Energy realeased in the condenser in domestic hot water mode [Wh]

        EauxSH = fill(0.0, number_steps);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]
        EauxDHW = fill(0.0, number_steps);                                                          # Energy for domestic hot water delivered to the house by the auxiliary system [Wh]

        WcompressorSH = fill(0.0, number_steps);                                                        # Compressor power in space heating mode [W]
        WcompressorDHW = fill(0.0, number_steps);                                                       # Compressor power in domestic hot water mode [W]
        EcompressorSH = fill(0.0, number_steps);                                                       # Energy used by the compressor for space heating [Wh]
        EcompressorDHW = fill(0.0, number_steps);                                                      # Energy used by the compressor for domestic hot water [Wh]

        ontimeSH = fill(0.0, number_steps);                                                             # Share of the hour while the HP is on for space heating [-]
        ontimeDHW = fill(0.0, number_steps);                                                            # Share of the hour while the HP is on for domestic hot water[-]

        auxonflagSH = fill(0.0, number_steps);                                                         # Flag indicating if the HP is used for spaced heating
        auxonflagDHW = fill(0.0, number_steps);                                                        # Flag indicating if the HP is used for domestic hot water

        newTbh = 0.;
        E_extra = 0.;
        AT_bh_extra = 0.;
        year = 0;
    ## end initialization

        for z = 1 : number_steps
            if Teva_in_hp[z] < T_threshold + 273.15   # if the ground is too cold run the auxiliary system
                auxonflagSH[z] = 2; # Auxiliary used for  SH
                auxonflagDHW[z] = 2; # Auxiliary used for DH
                EauxDHW[z] =  QloadDHW[z];
                EauxSH[z] = QloadSH[z];

                Esolar_to_HP[z] = min(Esolar, EauxDHW + EauxSH)
                Esolar_to_ground[z] = max(0., Esolar - EauxDHW -EauxSH) * ampl_factor

                if z < number_steps
                    if z % 8760 == 0
                        year = Int64(z/8760);
                        AT_bh_extra = sum((EgroundSH[1:z] .+ EgroundDHW[1:z]) .- Esolar_to_ground[1:z]) * 1e3 / (z * H) * influence[year]; # [K] Assumes the same linear heat extraction for all the boreholes (positive in case of heat extraction)
                    end
                    # TO DO: use load aggregation for the function "wall_temperature"
                    AT_bh = wall_temperature((EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .* 1e3, self_response); # [K] Positive in case of heat extraction
                    T_bh[z+1] = T_bh[1] - AT_bh - AT_bh_extra;
                    Teva_in_hp[z+1] = brine_temperature(T_bh[z+1], (EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .* 1e3, H, brine_flowrate_eva; T0 = T0 + 273.15, Rb = Rb, Brine = Brine)
                end
            else
                # DHW production
                HPperformance = predict_performance(nominal_brine_flowrate_eva, nominal_water_flowrate_cond, Teva_in_hp[z] - 273.15, Tcond_in_DHW[z] - 273.15, regrHC, regrCOP);
                QcondDHW[z] = HPperformance[1][1] * capacity_ratio; # [kW]
                COPDHW = HPperformance[2][1];
                WcompressorDHW[z] = QcondDHW[z] / COPDHW;           # [kW]
                QevaDHW[z] = QcondDHW[z] - WcompressorDHW[z];       # [kW]

                HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
                ontimeDHW[z] = HPdelivered[1];
                EcompressorDHW[z], EauxDHW[z] = HPdelivered[3], HPdelivered[4];
                EgroundDHW[z] = HPdelivered[2];
                EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;
                EcondDHW[z] = QcondDHW[z] * ontimeDHW[z];
                # SH production
                HPperformance = predict_performance(nominal_brine_flowrate_eva, nominal_water_flowrate_cond, Teva_in_hp[z] - 273.15, Tcond_in_SH[z], regrHC, regrCOP);
                QcondSH[z] = HPperformance[1][1] * capacity_ratio;
                COPSH = HPperformance[2][1];
                WcompressorSH[z] = QcondSH[z] / COPSH;
                QevaSH[z] = QcondSH[z] - WcompressorSH[z];

                HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);
                ontimeSH[z] = HPdelivered[1];
                EgroundSH[z] = HPdelivered[2];
                EcompressorSH[z] = HPdelivered[3];
                EauxSH[z] = HPdelivered[4];
                EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;
                EcondSH[z] = QcondSH[z] * ontimeSH[z];

                Esolar_to_HP[z] = min(Esolar[z], EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]);
                Esolar_to_ground[z] = max(0., Esolar[z] - EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]) * ampl_factor;

                if z < number_steps
                    if z % 8760 == 0
                        year = Int64(z/8760);
                        AT_bh_extra = sum(EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z]) * 1e3 / (z * H) * influence[year];
                    end
                    # TO DO: use load aggregation for the function "wall_temperature"
                    AT_bh = wall_temperature((EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .*1e3, self_response);
                    T_bh[z+1] = T_bh[1] - AT_bh - AT_bh_extra;
                    Teva_in_hp[z+1] = brine_temperature(T_bh[z+1], (EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .* 1e3, H, brine_flowrate_eva; T0 = T0 + 273.15, Rb=Rb, Brine = Brine)

                    if Teva_in_hp[z+1] < T_threshold + 273.15
                        diff = 1.;
                        while diff > 1e-2
                            E_extra = (T_threshold + 273.15 - T_bh[z+1] ) / (1/ 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", T_threshold + 273.15, Brine) - Rb/H) /1e3 # [kWh]
                            AT_bh = wall_temperature(vcat((EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z]), E_extra)./H .*1e3, self_response);
                            newTbh = T_bh[1] - AT_bh - AT_bh_extra;
                            diff = abs(newTbh - T_bh[z+1]) / newTbh;
                        end
                        T_bh[z+1] = newTbh;

                        if E_extra > EgroundDHW[z]
                            EgroundSH[z] = E_extra - EgroundDHW[z];
                        else
                            EgroundSH[z] = 0.;
                            EgroundDHW[z] = E_extra;
                        end
                        EcondDHW[z] = EgroundDHW[z] * COPDHW / (COPDHW-1);
                        EcondSH[z] = EgroundSH[z] * COPSH / (COPSH-1);
                        EauxDHW[z] = QloadDHW[z] - EcondDHW[z];
                        EauxSH[z] = QloadSH[z] - EcondSH[z];

                        EcompressorSH[z] = EcondSH[z] / COPSH;
                        EcompressorDHW[z] = EcondDHW[z] / COPDHW;

                        Esolar_to_HP[z] = min(Esolar[z], EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]);
                        Esolar_to_ground[z] = max(0., Esolar[z] - EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]) * ampl_factor;

                        Teva_in_hp[z+1] = T_threshold + 273.15;
                    end

                end
            end
        end
    return [EcompressorSH, EauxSH, QloadSH, EcompressorDHW, EauxDHW, QloadDHW, Esolar_to_HP, Esolar_to_ground, ontimeSH, ontimeDHW, auxonflagSH, auxonflagDHW, (EgroundSH .+ EgroundDHW), T_bh, Teva_in_hp];
end
"""
    groundPVheatpump_solo_recharge(n_years::Int, field_characteristics::Array{T,2}, H::T, borehole_extra_info::Array{T,1}, Esolar_1::Array{T,1}, ampl_factor::T; T_DHW = 50 + 273.15, DHW_filename = joinpath(pwd(), "data/DHW_load.jld"), load_filename= joinpath(pwd(),"data/Villa00.txt"), T0 = 8., heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv"), T_threshold = -8., nominal_capacity = 7.0, nominal_brine_flowrate_eva = 0.40, nominal_water_flowrate_cond = 0.16, Brine = "INCOMP::MEA-28%", Rb::T = 0.07, rb::T = 0.0575, D = 6., k = 3.1, ro = 2300., c_p = 870.) where {T <:AbstractFloat}


Simulate the operation of a ground-source heat pump for space heating and/or domestic hot water during `n_years` years taking into account the presence of neighbouring installations and
the recharging of the borehole heat exchagners

# Output:

1. `EcompressorSH` electricity used by the compressor in space heating mode [kWh]
2. `EauxSH` energy used by the auxiliary in space heating mode [kWh]
3. `QloadSH` space heating demand [kWh]
4. `EcompressorDHW` electricity used by the compressor in space heating mode [Wh]
5. `EauxDHW` electricity used by the auxiliary in domestic hot water mode [Wh]
6. `QloadDHW` hot water demand [kWh]
7. `Esolar_to_HP` solar electricity that covers the heating demand [kWh]
8. `Esolar_to_ground` solar electricity that recharges the ground [kWh]
9. `ontimeSH` share of the hour during which the HP operates in space heating mode [-]
10. `ontimeDHW` share of the hour during which the HP operates in domestic hot water mode [-]
11. `auxonflagSH` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in space heating mode [-]
12. `auxonflagDHW` set to 0 if the auxiliary is not used, 1 if the auxiliary is partially used, 2 if the auxiliary is the only heat source used in domestic hot water mode [-]
13. `(EgroundSH .+ EgroundDHW)` heat absorbed from the ground [kWh]
14. `T_bh` borehole wall temperature [K]
15. `Teva_in_hp` return temperature from the ground heat exchnager [K]

# Arguments:

1. `n_years` number of years to simulate [-]
2. `field_characteristics` matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `H` borehole length of the borehole of interest [m]
4. `borehole_extra_info` Vector (Array{T,1}!!!) containing [year of installation; x position; y position] of the borehole of interest;
5. `Esolar_1` vector containing the PV overproduction (after appliances) [kWh]
6. `ampl_factor` efficiency at which the solar electricity will be converted into heat for ground recharging

## Keyword arguments:
- `T_DHW` temperature of the return temperature from the domestic hot water production distribution system [K]
- `DHW_filename` absolute path to the file containing the domestic hot water load with hourly resolution for 1 year [kWh or Whis indifferent]
- `load_filename` absolute path to the file containing the space heating load with hourly resolution for 1 year [kWh or Wh is indifferent]
- `T0 ` initial ground temperature [°C]
- `heatpump_catalogue` absolute path to the file containing the csv file for the HP regression [kW, l/s, l/s, -]
- `T_threshold` temperature under which the HP turns off [°C]
- `nominal_capacity` nominal heating capacity of the heat pump [kW]
- `nominal_brine_flowrate_eva` nominal brine flow rate in the evaporator [l/s]
- ` nominal_water_flowrate_cond` nominal water flow rate in the condenser [l/s]
- `Brine` name of the brine in the evaporator
- `rb = 7.5e-2`;        # borehole radius [m]
- `D = 6.`;             # borehole buried depth
- `k = 3.1`;            # ground thermal conductivity [W/mK]
- `ro = 2300`;          # ground density [kg/m3]
- `c_p = 870`;          # ground heating capacity [J/(kg K)]
"""
    function groundPVheatpump_solo_recharge(n_years::Int, field_characteristics::Array{T,2}, H::T, borehole_extra_info::Array{T,1}, Esolar_1::Array{T,1}, ampl_factor::T; T_DHW = 50 + 273.15, DHW_filename = joinpath(pwd(), "data/DHW_load.jld"), load_filename= joinpath(pwd(),"data/Villa00.txt"), T0 = 8., heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv"), T_threshold = -8., nominal_capacity = 7.0, nominal_brine_flowrate_eva = 0.40, nominal_water_flowrate_cond = 0.16, Brine = "INCOMP::MEA-28%", Rb::T = 0.07, rb::T = 0.0575, D = 6., k = 3.1, ro = 2300., c_p = 870.) where {T <:AbstractFloat}
        # The simulation involves n_years with hourly resolution
            steps_per_year = 8760;                      # numbed of steps per years [-]
            timestep_length = 3600.;                    # length of a time step [s]
            number_steps = n_years * steps_per_year;    # total number of time steps [-]
        #-------------------------------------------------------
            borehole_characteristics = [D; H+D; H * 8760/1000];
            self_response = abs.(FLS_I(borehole_characteristics, number_steps, tstep = timestep_length, T_ground = 0., rb = rb))
        # The borehole of interest is always positioned in x,y coordinates 0, 0
            complete_borehole_characteristics = reshape(vcat(borehole_extra_info, borehole_characteristics),1, 6);
        #--------------------------------------------------------
            responses = FLS_I(field_characteristics, complete_borehole_characteristics, n_years, T_ground = 0, tstep = steps_per_year * timestep_length, k=k, ro=ro, cp=c_p, rb=rb)
            influence = abs.(responses[1][end,:] .- responses[3][end,:])
    # Determine the load of the house and HP connected to the borehole of interest
            annual_load_house = house_heating_demand(H);                # SH + DHW [kWh]
            (profile, TcondinSH) = profile_from_file(load_filename)     # [-, degC]
            rel_capacity = relative_hp_capacity(profile);               # [-]
            hp_cap = hp_capacity(rel_capacity, annual_load_house);      # [kWh]

            capacity_ratio = hp_cap / nominal_capacity;
            brine_flowrate_eva = nominal_brine_flowrate_eva * capacity_ratio;
    # Compute the regression models for the HP
        regressions = generate_regressions(heatpump_catalogue)
        regrHC = regressions[1];
        regrCOP = regressions[2];

        perDHW = 0.14;
        loadDHW = load(DHW_filename)["DHW_load"];
        profileDHW = loadDHW ./ sum(loadDHW)
        loadSH = annual_load_house * (1 - perDHW) .* profile; # [kWH]
        loadDHW = annual_load_house * perDHW .* profileDHW; # [kWH]
    ## Initializing variables
        T_bh = fill(0.0, number_steps);                                                        # Borehole wall temperature [K]
        T_bh[1] = T0 + 273.;                                                                    # Initial borehole wall temperature [K]
        Teva_in_hp = fill(0.0, number_steps);                                                  # Temperature of the fluid circulating in the borehole
        Teva_in_hp[1] = T_bh[1] - 3;                                                            # Initial return temperature of fluid circulating in the borehole 3 K lower than the borehole temperature. The best would be to find it iterating.

        Tcond_in_SH = repeat(TcondinSH, n_years);                                                     # Return temperature from the distribution system for space heating (water temperature at condenser inlet) [K]
        Tcond_in_DHW = fill(T_DHW, number_steps);                                                   # Return temperature from the distribution system for domestic hot water (water temperature at condenser inlet) [K]
        QloadSH = repeat(loadSH, n_years);                                                            # Heating demand for space heating [Wh]
        QloadDHW = repeat(loadDHW, n_years);                                                          # Heating demand for domestic hot water [Wh]

        Esolar = repeat(Esolar_1, n_years);
        Esolar_to_HP = fill(0.0, number_steps);
        Esolar_to_ground = fill(0.0, number_steps);

        QevaSH = fill(0.0, number_steps);                                                            # HP cooling capacity in space heating mode [W],
        TevaSH = fill(0.0, number_steps);                                                            # Refrigerant evaporation T in space heating mode [K]
        EevaSH = fill(0.0, number_steps);                                                            # Energy absorbed in the evaporator in space heating mode [Wh]
        EgroundSH = fill(0.0, number_steps);                                                         # Energy extracted from the BH in space heating mode [Wh]
        QevaDHW = fill(0.0, number_steps);                                                           # HP cooling capacity in domestic hot water mode [W],
        TevaDHW = fill(0.0, number_steps);                                                           # Refrigerant evaporation T in domestic hot water mode [K]
        EevaDHW = fill(0.0, number_steps);                                                           # Energy absorbed in the evaporator in domestic hot water mode [Wh]
        EgroundDHW = fill(0.0, number_steps);                                                        # Energy extracted from the BH in domestic hot water mode [Wh]

        QcondSH = fill(0.0, number_steps);                                                             # HP Heating capacity in space heating mode [W]
        TcondSH = fill(0.0, number_steps);                                                             # Refrigerant evaporation T in space heating mode [K]
        EcondSH = fill(0.0, number_steps);                                                             # Energy realeased in the condenser in space heating mode [Wh]
        QcondDHW = fill(0.0, number_steps);                                                             # HP Heating capacity in domestic hot water mode [W]
        TcondDHW = fill(0.0, number_steps);                                                             # Refrigerant evaporation T in domestic hot water mode [K]
        EcondDHW = fill(0.0, number_steps);                                                             # Energy realeased in the condenser in domestic hot water mode [Wh]

        EauxSH = fill(0.0, number_steps);                                                           # Energy for space heating delivered to the house by the auxiliary system [Wh]
        EauxDHW = fill(0.0, number_steps);                                                          # Energy for domestic hot water delivered to the house by the auxiliary system [Wh]

        WcompressorSH = fill(0.0, number_steps);                                                        # Compressor power in space heating mode [W]
        WcompressorDHW = fill(0.0, number_steps);                                                       # Compressor power in domestic hot water mode [W]
        EcompressorSH = fill(0.0, number_steps);                                                       # Energy used by the compressor for space heating [Wh]
        EcompressorDHW = fill(0.0, number_steps);                                                      # Energy used by the compressor for domestic hot water [Wh]

        ontimeSH = fill(0.0, number_steps);                                                             # Share of the hour while the HP is on for space heating [-]
        ontimeDHW = fill(0.0, number_steps);                                                            # Share of the hour while the HP is on for domestic hot water[-]

        auxonflagSH = fill(0.0, number_steps);                                                         # Flag indicating if the HP is used for spaced heating
        auxonflagDHW = fill(0.0, number_steps);                                                        # Flag indicating if the HP is used for domestic hot water

        newTbh = 0.;
        E_extra = 0.;
        AT_bh_extra = 0.;
        year = 0;
    ## end initialization

        for z = 1 : number_steps
            if Teva_in_hp[z] < T_threshold + 273.15   # if the ground is too cold run the auxiliary system
                auxonflagSH[z] = 2; # Auxiliary used for  SH
                auxonflagDHW[z] = 2; # Auxiliary used for DH
                EauxDHW[z] =  QloadDHW[z];
                EauxSH[z] = QloadSH[z];

                Esolar_to_HP[z] = min(Esolar, EauxDHW + EauxSH)
                Esolar_to_ground[z] = max(0., Esolar - EauxDHW -EauxSH) * ampl_factor

                if z < number_steps
                    if z % 8760 == 0
                        year = Int64(z/8760);
                        AT_bh_extra = sum((EgroundSH[1:z] .+ EgroundDHW[1:z])) * 1e3 / (z * H) * influence[year]; # [K] Assumes the same linear heat extraction for all the boreholes (positive in case of heat extraction)
                    end
                    # TO DO: use load aggregation for the function "wall_temperature"
                    AT_bh = wall_temperature((EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .* 1e3, self_response); # [K] Positive in case of heat extraction
                    T_bh[z+1] = T_bh[1] - AT_bh - AT_bh_extra;
                    Teva_in_hp[z+1] = brine_temperature(T_bh[z+1], (EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .* 1e3, H, brine_flowrate_eva; T0 = T0 + 273.15, Rb = Rb, Brine = Brine)
                end
            else
                # DHW production
                HPperformance = predict_performance(nominal_brine_flowrate_eva, nominal_water_flowrate_cond, Teva_in_hp[z] - 273.15, Tcond_in_DHW[z] - 273.15, regrHC, regrCOP);
                QcondDHW[z] = HPperformance[1][1] * capacity_ratio; # [kW]
                COPDHW = HPperformance[2][1];
                WcompressorDHW[z] = QcondDHW[z] / COPDHW;           # [kW]
                QevaDHW[z] = QcondDHW[z] - WcompressorDHW[z];       # [kW]

                HPdelivered = hp_onoff(QcondDHW[z], QevaDHW[z], WcompressorDHW[z], QloadDHW[z], 1.);
                ontimeDHW[z] = HPdelivered[1];
                EcompressorDHW[z], EauxDHW[z] = HPdelivered[3], HPdelivered[4];
                EgroundDHW[z] = HPdelivered[2];
                EauxDHW[z] > 0.0 ? auxonflagDHW[z] = 1 : auxonflagDHW[z] = 0;
                EcondDHW[z] = QcondDHW[z] * ontimeDHW[z];
                # SH production
                HPperformance = predict_performance(nominal_brine_flowrate_eva, nominal_water_flowrate_cond, Teva_in_hp[z] - 273.15, Tcond_in_SH[z], regrHC, regrCOP);
                QcondSH[z] = HPperformance[1][1] * capacity_ratio;
                COPSH = HPperformance[2][1];
                WcompressorSH[z] = QcondSH[z] / COPSH;
                QevaSH[z] = QcondSH[z] - WcompressorSH[z];

                HPdelivered = hp_onoff(QcondSH[z], QevaSH[z], WcompressorSH[z], QloadSH[z], 1. - ontimeDHW[z]);
                ontimeSH[z] = HPdelivered[1];
                EgroundSH[z] = HPdelivered[2];
                EcompressorSH[z] = HPdelivered[3];
                EauxSH[z] = HPdelivered[4];
                EauxSH[z] > 0.0 ? auxonflagSH[z] = 1 : auxonflagSH[z] = 0;
                EcondSH[z] = QcondSH[z] * ontimeSH[z];

                Esolar_to_HP[z] = min(Esolar[z], EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]);
                Esolar_to_ground[z] = max(0., Esolar[z] - EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]) * ampl_factor;

                if z < number_steps
                    if z % 8760 == 0
                        year = Int64(z/8760);
                        AT_bh_extra = sum(EgroundSH[1:z] .+ EgroundDHW[1:z]) * 1e3 / (z * H) * influence[year];
                    end
                    # TO DO: use load aggregation for the function "wall_temperature"
                    AT_bh = wall_temperature((EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .*1e3, self_response);
                    T_bh[z+1] = T_bh[1] - AT_bh - AT_bh_extra;
                    Teva_in_hp[z+1] = brine_temperature(T_bh[z+1], (EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z])./H .* 1e3, H, brine_flowrate_eva; T0 = T0 + 273.15, Rb=Rb, Brine = Brine)

                    if Teva_in_hp[z+1] < T_threshold + 273.15
                        diff = 1.;
                        while diff > 1e-2
                            E_extra = (T_threshold + 273.15 - T_bh[z+1] ) / (1/ 2 / brine_flowrate_eva / CoolProp.PropsSI("C", "P", 101325., "T", T_threshold + 273.15, Brine) - Rb/H) /1e3 # [kWh]
                            AT_bh = wall_temperature(vcat((EgroundSH[1:z] .+ EgroundDHW[1:z] .- Esolar_to_ground[1:z]), E_extra)./H .*1e3, self_response);
                            newTbh = T_bh[1] - AT_bh - AT_bh_extra;
                            diff = abs(newTbh - T_bh[z+1]) / newTbh;
                        end
                        T_bh[z+1] = newTbh;

                        if E_extra > EgroundDHW[z]
                            EgroundSH[z] = E_extra - EgroundDHW[z];
                        else
                            EgroundSH[z] = 0.;
                            EgroundDHW[z] = E_extra;
                        end
                        EcondDHW[z] = EgroundDHW[z] * COPDHW / (COPDHW-1);
                        EcondSH[z] = EgroundSH[z] * COPSH / (COPSH-1);
                        EauxDHW[z] = QloadDHW[z] - EcondDHW[z];
                        EauxSH[z] = QloadSH[z] - EcondSH[z];

                        EcompressorSH[z] = EcondSH[z] / COPSH;
                        EcompressorDHW[z] = EcondDHW[z] / COPDHW;

                        Esolar_to_HP[z] = min(Esolar[z], EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]);
                        Esolar_to_ground[z] = max(0., Esolar[z] - EcompressorDHW[z] + EcompressorSH[z] + EauxDHW[z] + EauxSH[z]) * ampl_factor;

                        Teva_in_hp[z+1] = T_threshold + 273.15;
                    end

                end
            end
        end
    return [EcompressorSH, EauxSH, QloadSH, EcompressorDHW, EauxDHW, QloadDHW, Esolar_to_HP, Esolar_to_ground, ontimeSH, ontimeDHW, auxonflagSH, auxonflagDHW, (EgroundSH .+ EgroundDHW), T_bh, Teva_in_hp];
end
