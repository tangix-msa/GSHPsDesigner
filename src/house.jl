"""
    house_heating_demand(H, linear_load_design=15, satisfied_energy=0.99, COP_design=3.2)

### Calculate the total yearly heating demand (space heating plus domestic hot
### water) of a building from the length of the borehole it is connected to.

# Output:

- `annual_load_house`: Total yearly heating demand [kWh/yr].

# Input:

- `H` : Borehole active length [m].

# Default arguments:
- `linear_load_design`: Average annual linear heat extraction from the borehole [W/m]
    Default is 15.
- `satisfied_energy`: Fraction of the heating demand satisfied by the heat pump (this
    exludes auxiliary heating).
    Default is 0.99.
- `COP_design` : Average annual COP of the heat pump.
    Default is 3.2.
"""
function house_heating_demand(H; linear_load_design=15, satisfied_energy=0.98, COP_design=3.2)
    annual_load_borehole = H * linear_load_design * 8.76 # [kWh]
    annual_load_house = annual_load_borehole * COP_design / (COP_design - 1) / satisfied_energy # [kWh]
    return annual_load_house
end
"""
    profile_from_file(filename)

### Return the relative** heating demand with hourly resolution for an year.
### Also returns the return temperature from the radiators.

** ratio between the heating demand during one hour and the total yearly heating demand.

# Output:
1. `Q_norm`: Fraction of the total annual heating demand at each hour.
2. `T_return`: Returning temperature from the radiators at each hour [degC].

# Input:

- `filename`: absolute path of the text file containing the loads and temperatures.
"""
function profile_from_file(filename)
    # Load data from file
    data = readdlm(filename)
    # Extract loads and returning temperatures
    Q_building = data[:,1]
    Q_norm = Q_building ./ sum(Q_building)
    T_return = data[:,2]
    return Q_norm, T_return
end
"""
    relative_hp_capacity(profile, satisfied_energy=0.99)

### Calculate the ratio between the needed heat pump capacity and total yearly heating demand.
### To be coupled with the function `hp_capacity`

# Output:

- `max_relative_capacity_needed`: ratio between the needed heat pump capacity and the total yearly heating demand

# Input:
----------
1. `profile`: relative** heating demand with hourly resolution for an year.
**ratio between the heating demand during one hour and the total yearly heating demand.
2. `satisfied_energy`: fraction of the heating demand satisfied by the heat pump (this
    excludes auxiliary heating). Default is 0.99.
"""
function relative_hp_capacity(profile::Array{T}; satisfied_energy::T = 0.98) where {T<:AbstractFloat}
    n_hours = 8760
    sorted_profile = sort(profile)                                            # [-]
    cumulated_relative_heating_demand = fill(0., length(sorted_profile))               # [-]
    for ii in 1 : n_hours
        cumulated_relative_heating_demand[ii] = sum(sorted_profile[1:ii])         # [-]
    end
    pos = findfirst(x->x>satisfied_energy,cumulated_relative_heating_demand)
    max_relative_capacity_needed = sorted_profile[pos]
    return max_relative_capacity_needed
end
"""
    hp_capacity(max_relative_capacity_needed::T, total_yearly_load::T) where {T<:AbstractFloat}

### Calculate the needed HP size (rounded up) (in kW)
### given the building yearly heating demand

# Output:
 - `capacity` : hp capacity (kW)

# Input:

`max_relative_capacity_needed`: ratio between the needed heat pump capacity and the total yearly heating demand
`total_yearly_load `: total yearly heating demand [kWh/yr].
"""
function hp_capacity(max_relative_capacity_needed::T, total_yearly_load::T) where {T<:AbstractFloat}
    capacity = ceil(max_relative_capacity_needed * total_yearly_load)
    return capacity
end
