"""
    temperature_penalty(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, tmax::Int64, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}

Calculate the influence of the existing boreholes on the planned borehole at time 'tmax'


# Output:

1. `T_penalty`: ground temperature change due to the neighbours [K]

# Arguments:

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `tmax: final`: timestep at which to calculate the temperature [year if the default timestep is used]


## Keyword arguments (optional):

- `T_ground = 8`        # initial ground temperature [°C]
- `k = 3.1`;            # ground thermal conductivity [W/mK]
- `ro = 2300`;          # ground density [kg/m3]
- `cp = 870`;           # ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`;        # borehole radius [m]
"""
function temperature_penalty(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax; T_ground = 8., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where{T<:AbstractFloat}
    new_borehole_characteristics[6] = 0;
    T_penalty = T_ground - FLS_I(field_characteristics, new_borehole_characteristics, tmax; T_ground = T_ground, k = k, ro = ro, cp = cp, rb = rb)[1][end,end];

    return T_penalty
end

"""
    minimum_temperature(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, tmax::Int64, q_year::T, q_month::T, q_hours::T, dT_fluid::T=3, T_ground::T=8, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, Rb::T= 0.07) where{T<:AbstractFloat}

Calculate the temperature of the return fluid from the borehole after several years of operation, taking into account 1 month peak load and 6 hours peak load.
(It follows the ASHRAE sizing method.)

# Arguments

- field_characteristics: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, xpos, ypos, D, H, linear load [W/m]]
- new_borehole_characteristics: matrix containing the following characteristics for the planned borehole: [year of installation, xpos, ypos, D, H, linear load [W/m]]
- tmax: final timestep at which to calculate the temperature [year]
- q_year: yearly heat load [W]
- q_month: peak monthly load [W]
- q_hours: peak 6 hours load [W]

## Keywords arguments
- dT_fluid = 3;  fluid temperature change between borehole inlet and outlet [K]
- T_ground = 8;  undisturbed ground temperature [C]
- k = 3.1;       ground thermal conductivity [W/mK]
- ro = 2300;     ground density [kg/m3]
- cp = 870;      ground heating capacity [J/(kg K)]
- rb = 7.5e-2;   borehole radius [m]
- Rb = 0.07      borehole thermal resistance [m K/W]
"""
function minimum_temperature(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int, q_year::T, q_month::T, q_hours::T; dT_fluid::T=3., T_ground::T=8., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, Rb::T= 0.07) where{T<:AbstractFloat}

    T_penalty = temperature_penalty(field_characteristics, new_borehole_characteristics, tmax, k = k, ro = ro, cp = cp, rb = rb)
    αg = k/ro/cp;

    t_years = tmax * 8760 * 3600.;
    t_month = 30 * 24 * 3600.;
    t_hours =  6 * 3600.;
    t_total = t_years + t_month + t_hours;

    D = new_borehole_characteristics[4];
    H = new_borehole_characteristics[5] - new_borehole_characteristics[4];

    g_total = fls([t_total], αg, rb, D, D, H, H);
    g_monthandhours = fls([t_month + t_hours], αg, rb, D, D, H, H);
    g_hours = fls([t_hours], αg, rb, D, D, H, H);

    R_year = 1 / (2 * pi * k) * (g_total - g_monthandhours);
    R_month = 1 / (2 * pi * k) * (g_monthandhours - g_hours);
    R_hours = 1 / (2 * pi * k) * g_hours;

    Tm = T_ground + T_penalty + 1/H * (q_year * R_year[1] + q_month * R_month[1] + q_hours * R_hours[1])
    Tout = Tm + dT_fluid/2;

    return Tout
end
"""
    fluid_temperature(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, loadbh::Array{Float64,2}, tmax::Int64; m_fluid::T=0.5,T_ground::T=8., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, Rb::T= 0.07, m::Int=12) where{T<:AbstractFloat}

## Keywords arguments
- dT_fluid = 3.;  fluid temperature change between borehole inlet and outlet [K]
- T_ground = 8.;  undisturbed ground temperature [C]
- k = 3.1;       ground thermal conductivity [W/mK]
- ro = 2300;     ground density [kg/m3]
- cp = 870;      ground heating capacity [J/(kg K)]
- rb = 7.5e-2;   borehole radius [m]
- Rb = 0.07      borehole thermal resistance [m K/W]
"""
function fluid_temperature(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, loadbh::Array{T,2}, tmax::Int; m_fluid::T=0.5,T_ground::T=8., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2, Rb::T= 0.07) where{T<:AbstractFloat}

    αg = k/ro/cp;
    T_penalty = temperature_penalty(field_characteristics, new_borehole_characteristics, tmax, T_ground = T_ground, k = k, ro = ro, cp = cp, rb = rb)
    t = collect(1 : tmax * 8760) .* 3600.;

    self_response = 1/(2*pi*k) .* fls(t, αg, rb, new_borehole_characteristics[4], new_borehole_characteristics[4], new_borehole_characteristics[5], new_borehole_characteristics[5]);

    dT_self = fill(0.,8760,1);
    Twall = fill(0.,8760,1);
    Tbrine_out = fill(0.,8760,1)

    T_ground_disturbed = T_ground - T_penalty;
    load_convolution =  loadbh .* 1000 ./ (new_borehole_characteristics[5] - new_borehole_characteristics[4]) ;
    load_convolution_diff = fill(0.,8760,1);
    load_convolution_diff[1] = load_convolution[1] - new_borehole_characteristics[6] * 1000/8760 / (new_borehole_characteristics[5] - new_borehole_characteristics[4]);
    for ii = 2 : 8760
        load_convolution_diff[ii] = load_convolution[ii] - load_convolution[ii-1]
    end

    for z = 1 : 8760
        dT_self[z] = self_dT(load_convolution_diff[1:z], self_response); # Self-induced temperature change on the borehole wall [K]
        Twall[z] = T_ground_disturbed - dT_self[z] - self_response[(tmax-1)*8760 + z] * (new_borehole_characteristics[6] * 1000/8760 / (new_borehole_characteristics[5] - new_borehole_characteristics[4]))
        Tbrine_out[z] = Twall[z] - loadbh[z]/(new_borehole_characteristics[5] - new_borehole_characteristics[4])  * Rb + 0.5 * loadbh[z]/m_fluid/4200. ;
    end
    return Twall, Tbrine_out
end

function self_dT(q_diff::Array{T,1}, fls::Array{T,1})::T where {T<:AbstractFloat}
       steps = length(q_diff);
       sum = 0.;
       # commented code to be used (after checking) if the load and not load diff is used
       # for kk = 1 : steps-1
       #         sum = sum + q[kk] * (fls[(steps) - kk + 1] - fls[(steps) - kk]);
       # end
       for kk = 1 : steps
               sum = sum + q_diff[kk] * (fls[steps-kk+1]);
       end
       return sum
end
