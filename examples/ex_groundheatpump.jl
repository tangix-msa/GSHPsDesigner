# Initiaization
    n = 2;  # number of years to simlate
    H = 100.; # length of the borehole of interest
    borehole_extra_info = [1.; 0.; 0.]; # year of instaation and carteesian coordinates of the borehole of interest

    # Characteristics of the other boreholes: [year of installation, x coordinate, y coordinate, buried depth (use 6 if not available), borehole length, (length - buried depth)*8760/1000]
    field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                            1. 20. 0. 4. 184. 180*8760/1000;
                            1. 45. 0.0 6.0 306. 300*8760/1000;
                            1. 2. 25. 2.0 172. 170*8760/1000;
                            1. 5. 50. 1.0 201. 200*8760/1000;
                            1. 20. 40. 5.0 255. 250*8760/1000];

# Simulation
    res = groundheatpump_I(n, field_characteristics, H, borehole_extra_info);

### Post-processing

    # Time interval for which the results will be plotted
        interval = collect(1 : 8760);
    # Plot the electricity consumption for compressor and auxiliary, and the demand for Space Heating
        plot(res[1][interval])
        plot!(res[2][interval])
        plot!(res[3][interval])
    # COP
        plot(res[3][interval] ./ res[1][interval])

    # Plot the electricity consumption for compressor and auxiliary, and the demand for Domestic Hot Water
        interval = collect(1:24)
        plot(res[4][interval])
        plot!(res[5][interval])
        plot!(res[6][interval])

    # Plot the brine and borehole wall temperature
        plot(res[12][interval] .- 273)
        plot!(res[13][interval] .- 273)

###
n = 5;  # number of years
H = 100.;
borehole_extra_info = [1.; 0.; 0.];

field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                        1. 20. 0. 4. 184. 180*8760/1000;
                        1. 45. 0.0 6.0 306. 300*8760/1000;
                        1. 2. 25. 2.0 172. 170*8760/1000;
                        1. 5. 50. 1.0 201. 200*8760/1000;
                        1. 20. 40. 5.0 255. 250*8760/1000];

Esolar = fill(3., 8760)
ampl_factor = 1.;

res3x1 = groundPVheatpump(n, field_characteristics, H, borehole_extra_info, Esolar, ampl_factor)

# plot(res0[end] .- 273)
# plot!(res0[end-1] .- 273)
# # plot!(res0_5[end] .- 273)
# # plot!(res0_5[end-1] .- 273)
# plot(res0_5x5[end] .- 273)
# plot!(res0_5x5[end-1] .- 273)
#
interval = collect(1+8760*4 : 8760*5);

plot(res0_1x1[1][interval])
plot!(res0_1x1[2][interval])
plot!(res0_1x1[3][interval])


plot!(res0_5x1[1][interval])
plot!(res0_5x1[2][interval])
plot!(res0_5x1[3][interval])

plot!(res3x1[1][interval])
plot!(res3x1[2][interval])
plot!(res3x1[3][interval])

sum(res0_1x1[1] .+ res0_1x1[2] .+ res0_1x1[4] .+ res0_1x1[5])
sum(res3x1[1] .+ res3x1[2] .+ res3x1[4] .+ res3x1[5])

plot(res0_1x1[end-1][interval])
plot!(res3x1[end-1][interval])
#
# plot(res0_5x5[3][interval] ./ res0_5x5[1][interval])


################

n = 2;  # number of years
H = 100.;
borehole_extra_info = [1.; 0.; 0.];

field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                        1. 20. 0. 4. 184. 180*8760/1000;
                        1. 45. 0.0 6.0 306. 300*8760/1000;
                        1. 2. 25. 2.0 172. 170*8760/1000;
                        1. 5. 50. 1.0 201. 200*8760/1000;
                        1. 20. 40. 5.0 255. 250*8760/1000];

DHW_filename = joinpath(pwd(), "data/DHW_load.jld")
# DHW_filename = joinpath(pwd(), "data/DHW_load.jld")
loadDHW = load(DHW_filename)["DHW_load"];
profileDHW = vec(loadDHW ./ sum(loadDHW));
SH_filename = joinpath(pwd(), "data/Villa00.txt")
(profileSH, TcondinSH) = profile_from_file(SH_filename)              # [-, degC]
perDHW = 0.14;
rel_capacity = relative_hp_capacity(profileSH * (1 - perDHW) .+ profileDHW * perDHW);

heatpump_catalogue = joinpath(pwd(), "data/ETON6_Ethanol28_extrapolated_catalogue.csv")
# heatpump_catalogue = joinpath(pwd(), "data/ETON6_Ethanol28_extrapolated_catalogue.csv")
regressions = generate_regressions(heatpump_catalogue)
regrHC = regressions[1];
regrCOP = regressions[2];

res = groundheatpump_I(n, field_characteristics, H, borehole_extra_info, profileDHW, profileSH, TcondinSH, rel_capacity, regrHC, regrCOP)

interval = collect(1 : 8760);

plot(res0_1x1[1][interval])
plot!(res0_1x1[2][interval])
plot!(res0_1x1[3][interval])
