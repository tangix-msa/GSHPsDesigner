## FLS_I(borehole_characteristics::Array{T,1}, tmax::Int; T_ground = 8., tstep::T=3600., k::T=3.1, ro::T=2300., c_p::T=870., rb::T=5.75e-2) where{T<:AbstractFloat}

D = 6.;
H = 100.;
tmax = 8760;
borehole_characteristics = [D; H+D; 1. * H *8760/1000];

results  = FLS_I(borehole_characteristics,  tmax, tstep = 3600., T_ground = 0., rb = 0.0575)

##

# field_characteristics= [year of installation, x cordinate, y coordinate , buried depth, totald length, linear heat extraction]

field_characteristics = [2010. 0. 0. 2. 152. 10. *150*8760/1000;
                        2005. 20. 0. 4. 184. 15. * 180*8760/1000;
                        2007. 45. 0.0 6.0 306. 12. *300*8760/1000;
                        2004. 2. 25. 2.0 172. 17. *170*8760/1000;
                        2001. 5. 50. 1.0 201. 20. *200*8760/1000;
                        2001. 20. 40. 5.0 255. 13. *250*8760/1000];

new_borehole_characteristics = [2015. 30. 30. 3. 243. 14. * 240 * 8760 / 1000];

time = 20; # number of years for which we want to forecast the temperature evolution in the underground (on the borehole walls)

scatter(vcat(field_characteristics[:,2], new_borehole_characteristics[:,2]) , vcat(field_characteristics[:,3], new_borehole_characteristics[:,3]) )

results  = FLS_I(field_characteristics, new_borehole_characteristics, time);
temperature_with_new_installation = results[1]
temperature_without_new_installation = results[2]
temperature_new_borehole_if_undisturbed = results[3]

#_____________________________________________________________________________

field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 + 4.82105 2136. *8760/1000
2001.0  68.7869  43.2332  4.99393  291.643 + 4.99393 3335. *8760/1000
2003.0  96.8925  17.2583  6.78014  218.149 + 6.78014 2157. *8760/1000
2004.0  37.8176  12.6237  6.99447  197.911 + 6.99447 1986. *8760/1000
2006.0  51.5132  66.6104  5.41417  290.921 + 5.41417 3489. *8760/1000
2007.0  73.5686  64.1044  7.22745  163.86 + 7.22745 1820. *8760/1000
2009.0  78.1709  92.0879  4.41364  296.348 + 4.41364 2876. *8760/1000
2012.0  37.0685  93.7397  7.88411  245.807 + 7.88411 2809. *8760/1000
2018.0  65.0639  13.197   5.04953  158.442 + 5.04953 1732. *8760/1000];

new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 + 7.03478 1498. *8760/1000];

time = 20; # number of years for which we want to forecast the temperature evolution in the underground (on the borehole walls)

scatter(vcat(field_characteristics[:,2], new_borehole_characteristics[:,2]) , vcat(field_characteristics[:,3], new_borehole_characteristics[:,3]) )

results  = FLS_I(field_characteristics, new_borehole_characteristics, time)
temperature_with_new_installation = results[1]
temperature_without_new_installation = results[2]
temperature_new_borehole_if_undisturbed = results[3]

# FLS_I(file_name::String)
filename = "Example.xlsx"
results = FLS_I(filename)
