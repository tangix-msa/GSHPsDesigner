# Careful with df_file, the directory changes from computer to computer!!
df_file = "/usr/myapp//data/ETON6_extrapolated_catalogue.csv"
res = generate_regressions(df_file)
regrHC = res[1]
regrCOP = res[2]

res = predict_performance(0.3, 0.3, -6., 40., regrHC, regrCOP)

HC = res[1][1]
COP = res[2][1]
