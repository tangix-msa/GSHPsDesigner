# The only purpous of the test is to check that the code does not break. These results are not validated, but make sense
    field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 2136. / 242.242
    2001.0  68.7869  43.2332  4.99393  291.643 3335. / 291.643
    2003.0  96.8925  17.2583  6.78014  218.149 2157. / 218.149
    2004.0  37.8176  12.6237  6.99447  197.911 1986. / 197.911
    2006.0  51.5132  66.6104  5.41417  290.921 3489. / 290.921
    2007.0  73.5686  64.1044  7.22745  163.86 1820. / 163.86
    2009.0  78.1709  92.0879  4.41364  296.348 2876. / 296.348
    2012.0  37.0685  93.7397  7.88411  245.807 2809. / 245.807
    2018.0  65.0639  13.197   5.04953  158.442 1732. / 158.442];
    new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 1498. / 175.695];

    "1st case: the temperature threshold is overcome immediately"
    dT_max = 0.;
    res1 = @capture_out(sustainabletime(field_characteristics, new_borehole_characteristics, dT_max));
    @test  res1 == "This neighbourhood will operate sustainably for  1 years";

    "2nd case: the temperature threshold is overcome after very long time"
    dT_max = 20.;
    res2 = @capture_out(sustainabletime(field_characteristics, new_borehole_characteristics, dT_max));
    @test  res2 == "This neighbourhoood will operate sustainably for at least 50 years";

    "3rd case: the temperature threshold is overcome after a few years from the installation"
    dT_max = 6.;
    res3 = @capture_out(sustainabletime(field_characteristics, new_borehole_characteristics, dT_max));
    @test  res3 == "This neighbourhood will operate sustainably for 26 years";

###

new_field_characteristics = [2020.0  37.8176  12.6237  6.99447  197.911 1986. / 197.911
    2020.0  51.5132  66.6104  5.41417  290.921 3489. / 290.921
    2020.0  73.5686  64.1044  7.22745  163.86 1820. / 163.86
    2020.0  78.1709  92.0879  4.41364  296.348 2876. / 296.348
    2020.0  37.0685  93.7397  7.88411  245.807 2809. / 245.807
    2020.0  65.0639  13.197   5.04953  158.442 1732. / 158.442];

dT_max = 0.;
res1 = @capture_out(sustainabletime(field_characteristics, new_borehole_characteristics, new_field_characteristics, dT_max));
@test res1 == "This neighbourhood will operate sustainably for  1 years";

dT_max = 20.;
res2 = @capture_out(sustainabletime(field_characteristics, new_borehole_characteristics, new_field_characteristics, dT_max));
@test res2 == "This neighbourhoood will operate sustainably for at least 50 years";

dT_max = 8.;
res3 = @capture_out(sustainabletime(field_characteristics, new_borehole_characteristics, new_field_characteristics, dT_max));
@test  res3 == "This neighbourhood will operate sustainably for  2 years";
