n = 2;  # number of years
H = 100.;
borehole_extra_info = [1.; 0.; 0.];

field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                        1. 20. 0. 4. 184. 180*8760/1000;
                        1. 45. 0.0 6.0 306. 300*8760/1000;
                        1. 2. 25. 2.0 172. 170*8760/1000;
                        1. 5. 50. 1.0 201. 200*8760/1000;
                        1. 20. 40. 5.0 255. 250*8760/1000];

res = groundheatpump_I(n, field_characteristics, H, borehole_extra_info, DHW_filename = joinpath(pwd(), "data/DHW_load.jld"), load_filename= joinpath(pwd(),"data/Villa00.txt"),  heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv"), T_threshold = -2.);

El_calculated = sum(res[1] .+ res[2] .+ res[4] .+ res[5]);








n = 2;  # number of years
H = 100.;
borehole_extra_info = [1.; 0.; 0.];

field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                        1. 20. 0. 4. 184. 180*8760/1000;
                        1. 45. 0.0 6.0 306. 300*8760/1000;
                        1. 2. 25. 2.0 172. 170*8760/1000;
                        1. 5. 50. 1.0 201. 200*8760/1000;
                        1. 20. 40. 5.0 255. 250*8760/1000];

res = groundheatpump_I(n, field_characteristics, H, borehole_extra_info, DHW_filename = joinpath(pwd(), "data/DHW_load.jld"), load_filename= joinpath(pwd(),"data/Villa00.txt"),  heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv"), T_threshold = -2.);

El_calculated = sum(res[1] .+ res[2] .+ res[4] .+ res[5]);
El_correct = 17303;

T_bh_correct_end = [272.5740964002404; 272.5755937729644; 272.56752172274025; 272.532901420601; 272.55170677082396];
T_bh_calculated_end = res[12][end-4:end];

T_brine_correct_end = [271.15; 271.15; 271.15; 271.15; 271.15];
T_brine_calculated_end = res[13][end-4:end];

@test abs(El_correct - El_calculated) / El_correct < 1e-2
@test maximum(abs.(T_bh_calculated_end .- T_bh_correct_end) / T_bh_correct_end) < 1e-2
@test maximum(abs.(T_brine_calculated_end .- T_brine_correct_end) / T_brine_correct_end) < 1e-2


#### to work on

n = 2;  # number of years
H = 100.;
borehole_extra_info = [1.; 0.; 0.];

field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                        1. 20. 0. 4. 184. 180*8760/1000;
                        1. 45. 0.0 6.0 306. 300*8760/1000;
                        1. 2. 25. 2.0 172. 170*8760/1000;
                        1. 5. 50. 1.0 201. 200*8760/1000;
                        1. 20. 40. 5.0 255. 250*8760/1000];

DHW_filename = joinpath(pwd(), "data/DHW_load.jld")
# DHW_filename = joinpath(pwd(), "data/DHW_load.jld")
loadDHW = load(DHW_filename)["DHW_load"];
profileDHW = loadDHW ./ sum(loadDHW);
SH_filename = joinpath(pwd(),"data/Villa00.txt")
(profileSH, TcondinSH) = profile_from_file(SH_filename)              # [-, degC]
perDHW = 0.14;
rel_capacity = relative_hp_capacity(profileSH * (1 - perDHW) + profileDHW * perDHW);

heatpump_catalogue = joinpath(pwd(),"data/ETON6_Ethanol28_extrapolated_catalogue.csv")
# heatpump_catalogue = joinpath(pwd(), "data/ETON6_Ethanol28_extrapolated_catalogue.csv")
regressions = generate_regressions(heatpump_catalogue)
regrHC = regressions[1];
regrCOP = regressions[2];

res = groundheatpump_I(n, field_characteristics, H, borehole_extra_info, profileDHW, profileSH, TcondinSH, rel_capacity, regrHC, regrCOP)

El_calculated = sum(res[1] .+ res[2] .+ res[4] .+ res[5]);
El_correct = 17303;

T_bh_correct_end = [272.5740964002404; 272.5755937729644; 272.56752172274025; 272.532901420601; 272.55170677082396];
T_bh_calculated_end = res[12][end-4:end];

T_brine_correct_end = [271.15; 271.15; 271.15; 271.15; 271.15];
T_brine_calculated_end = res[13][end-4:end];

@test abs(El_correct - El_calculated) / El_correct < 1e-2
@test maximum(abs.(T_bh_calculated_end .- T_bh_correct_end) / T_bh_correct_end) < 1e-2
@test maximum(abs.(T_brine_calculated_end .- T_brine_correct_end) / T_brine_correct_end) < 1e-2
