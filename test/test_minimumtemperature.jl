# The only purpous of the test is to check that the code does not break. These results are not validated. They make sense though.


field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 + 4.82105 2136. *8760/1000
    2001.0  68.7869  43.2332  4.99393  291.643 + 4.99393 3335. *8760/1000
    2003.0  96.8925  17.2583  6.78014  218.149 + 6.78014 2157. *8760/1000
    2004.0  37.8176  12.6237  6.99447  197.911 + 6.99447 1986. *8760/1000
    2006.0  51.5132  66.6104  5.41417  290.921 + 5.41417 3489. *8760/1000
    2007.0  73.5686  64.1044  7.22745  163.86 + 7.22745 1820. *8760/1000
    2009.0  78.1709  92.0879  4.41364  296.348 + 4.41364 2876. *8760/1000
    2012.0  37.0685  93.7397  7.88411  245.807 + 7.88411 2809. *8760/1000
    2018.0  65.0639  13.197   5.04953  158.442 + 5.04953 1732. *8760/1000];
    new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 + 7.03478 1498. *8760/1000];
    tmax = 20;

## temperature_penalty
    T_p = temperature_penalty(field_characteristics, new_borehole_characteristics, tmax)
    correct = 1.103
    @test  abs(T_p - correct)/correct < 1e-3


## minimum_temperature
    q_year = -2000.;
    q_month = -6000.;
    q_hours = -18000.;

    Tm = minimum_temperature(field_characteristics, new_borehole_characteristics, tmax, q_year, q_month, q_hours)
    correct = -1.941
    @test  abs(Tm - correct)/correct < 1e-3
