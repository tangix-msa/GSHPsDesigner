# test function FLS_I(field_characteristics::Array{Float64,2}, tmax::Int, rb::T=7.5e-2, k::T=3.1, ro::T=2300, cp::T=870) where{T<:AbstractFloat}
   # This test checks the results against the results obtained using 1 segment in the code used to generate the results for:
   # FASCÌ, MARIA LETIZIA, et al. "Simulation of thermal influence between independent geothermal boreholes in densely populated areas." Applied Thermal Engineering (2021): 117241.

   field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 + 4.82105 2136. * 8760/1000
   2001.0  68.7869  43.2332  4.99393  291.643 + 4.99393  3335. * 8760/1000
   2003.0  96.8925  17.2583  6.78014  218.149 + 6.78014 2157. * 8760/1000
   2004.0  37.8176  12.6237  6.99447  197.911 + 6.99447 1986. * 8760/1000
   2006.0  51.5132  66.6104  5.41417  290.921 + 5.41417 3489. * 8760/1000
   2007.0  73.5686  64.1044  7.22745  163.86 + 7.22745 1820. * 8760/1000
   2009.0  78.1709  92.0879  4.41364  296.348 + 4.41364 2876. * 8760/1000
   2012.0  37.0685  93.7397  7.88411  245.807 + 7.88411 2809. * 8760/1000
   2018.0  65.0639  13.197   5.04953  158.442 + 5.04953 1732. * 8760/1000
   2020. 15.8655  97.6883  7.03478  175.695 + 7.03478 1498. * 8760/1000];

   time = 20; # number of years for which we want to forecast the temperature evolution in the underground (on the borehole walls)

   results  = FLS_I(field_characteristics, time)
   computed = results[:,end];

   exact = 8 .- [3.8669972391220173, 5.443565103337566, 4.239015662990931, 4.639435641252562, 5.6705833459188275, 5.743247694819026, 4.300359004799124, 5.056626195711154, 5.096144190311241, 3.81732143693];
   @test maximum(abs.(computed .- exact) ./ exact) <= 1e-3;
# end test
 # test function FLS_I(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, tmax::Int, rb::T=7.5e-2, k::T=3.1, ro::T=2300, cp::T=870) where{T<:AbstractFloat}
    # This test checks the results against the results showed in fig.10 (up in) for log(t/ts) = [-3.9 0 3.9]
    # Cimmino, M., Bernier, M., Adams, F., 2013. A contribution towards the determination of g-functions using the finite line source. Applied Thermal Engineering 51, 401–412. https://doi.org/10.1016/j.applthermaleng.2012.07.044

     field_characteristics = [2010. 0. 0. 4. 104. 4*pi*100*8760/1000;
                             2010. 5. 0. 4. 104. 4*pi*100*8760/1000;
                             2010. 10. 0. 4. 104. 4*pi*100*8760/1000;
                             2010. 0. 5. 4. 104. 4*pi*100*8760/1000;
                             2010. 5. 5. 4. 104. 4*pi*100*8760/1000];
     new_borehole_characteristics = [2010. 10. 5. 4. 104. 4*pi*100*8760/1000];

     borehole_radius= 0.05;
     conductivity = 2.;
     alfa = 1e-6;
     density = 1000.;
     specific_heat = conductivity/ alfa / density;

     ts = 1e4/9/alfa; # 1e4 = 100^2 = H^2
     tmax = Int64(floor(ts * exp(4)/ 8760 / 3600));
     computed = FLS_I(field_characteristics, new_borehole_characteristics, tmax, k = conductivity, ro = density, cp = specific_heat, rb = borehole_radius)
     computed1 = mean(computed[1], dims =1)[1]
     computed2 = mean(computed[1], dims =1)[35]
     computed3 = mean(computed[1], dims =1)[end]
     exact1 = 8 .- 7.052799933398415;
     exact2 = 8 .- 14.483904132342976;
     exact3 = 8 .- 16.181331015801085;

     @test abs(computed1 - exact1) / exact1 <= 1e-3;
     @test abs(computed2 - exact2) / exact2 <= 1e-3;
     @test abs(computed3 - exact3) / exact3 <= 1e-3;
 # end test
 # test function FLS_I(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, tmax::Int, rb::T=7.5e-2, k::T=3.1, ro::T=2300, cp::T=870) where{T<:AbstractFloat}
    # This test checks the results against the results obtained using 1 segment in the code used to generate the results for:
    # FASCÌ, MARIA LETIZIA, et al. "Simulation of thermal influence between independent geothermal boreholes in densely populated areas." Applied Thermal Engineering (2021): 117241.

    field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 + 4.82105 2136. * 8760/1000
    2001.0  68.7869  43.2332  4.99393  291.643 + 4.99393  3335. * 8760/1000
    2003.0  96.8925  17.2583  6.78014  218.149 + 6.78014 2157. * 8760/1000
    2004.0  37.8176  12.6237  6.99447  197.911 + 6.99447 1986. * 8760/1000
    2006.0  51.5132  66.6104  5.41417  290.921 + 5.41417 3489. * 8760/1000
    2007.0  73.5686  64.1044  7.22745  163.86 + 7.22745 1820. * 8760/1000
    2009.0  78.1709  92.0879  4.41364  296.348 + 4.41364 2876. * 8760/1000
    2012.0  37.0685  93.7397  7.88411  245.807 + 7.88411 2809. * 8760/1000
    2018.0  65.0639  13.197   5.04953  158.442 + 5.04953 1732. * 8760/1000];
    new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 + 7.03478 1498. * 8760/1000];

    time = 20; # number of years for which we want to forecast the temperature evolution in the underground (on the borehole walls)

    results  = FLS_I(field_characteristics, new_borehole_characteristics, time)
    computed = results[1][:,end];

    exact = 8 .- [3.8669972391220173, 5.443565103337566, 4.239015662990931, 4.639435641252562, 5.6705833459188275, 5.743247694819026, 4.300359004799124, 5.056626195711154, 5.096144190311241, 3.81732143693];
    @test maximum(abs.(computed .- exact) ./ exact) <= 1e-3;
 # end test

 # test

  # This test checks the results against the results showed in fig.10 (up in) for log(t/ts) = [-3.9 0 3.9]
    # Cimmino, M., Bernier, M., Adams, F., 2013. A contribution towards the determination of g-functions using the finite line source. Applied Thermal Engineering 51, 401–412. https://doi.org/10.1016/j.applthermaleng.2012.07.044

    field_characteristics = [2010. 0. 0. 4. 104. 4*pi*100*8760/1000;
                             2010. 5. 0. 4. 104. 4*pi*100*8760/1000;
                             2010. 5. 5. 4. 104. 4*pi*100*8760/1000];
     new_borehole_characteristics = [2010. 10. 5. 4. 104. 4*pi*100*8760/1000];

     new_field_characteristics =  [ 20120. 10. 0. 4. 104. 4*pi*100*8760/1000;
                                  2030 0. 5. 4. 104. 4*pi*100*8760/1000];

     rb = 0.05;
     conductivity = 2.;
     alfa = 1e-6;
     density = 1000.;
     cp = conductivity / alfa / density;

     ts = 1e4/9/alfa; # 1e4 = 100^2 = H^2
     tmax = Int64(floor(ts * exp(4)/ 8760 / 3600));
     computed = FLS_I(field_characteristics, new_borehole_characteristics, new_field_characteristics, tmax, k = conductivity, ro = density, cp = specific_heat, rb = borehole_radius)
     computed1 = mean(computed[4], dims =1)[1]
     computed2 = mean(computed[4], dims =1)[35]
     computed3 = mean(computed[4], dims =1)[end]
     exact1 =  8 .- 7.052799933398415;
     exact2 =  8 .- 14.483904132342976;
     exact3 =   8 .- 16.181331015801085;

     @test abs(computed1 - exact1) / exact1 <= 1e-3;
     @test abs(computed2 - exact2) / exact2 <= 1e-3;
     @test abs(computed3 - exact3) / exact3 <= 1e-3;
 # end test
