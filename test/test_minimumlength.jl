# The only purpous of the test is to check that the code does not break. These results are not validated. The make sense though.
    field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 + 4.82105  2136. *8760/1000
    2001.0  68.7869  43.2332  4.99393  291.643 + 4.99393 3335. *8760/1000
    2003.0  96.8925  17.2583  6.78014  218.149 + 6.78014 2157. *8760/1000
    2004.0  37.8176  12.6237  6.99447  197.911 + 6.99447  1986. *8760/1000
    2006.0  51.5132  66.6104  5.41417  290.921 + 5.41417 3489. *8760/1000
    2007.0  73.5686  64.1044  7.22745  163.86 + 7.22745 1820. *8760/1000
    2009.0  78.1709  92.0879  4.41364  296.348 + 4.41364 2876. *8760/1000
    2012.0  37.0685  93.7397  7.88411  245.807 + 7.88411 2809. *8760/1000
    2018.0  65.0639  13.197   5.04953  158.442 + 5.04953 1732. *8760/1000];
    new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 + 7.03478  1498. *8760/1000];
    tmax = 20;

    "1st case: the temperature threshold is overcome even without the new boreholes"
    dT_max = 5.71;
    res1 = @capture_out(minimumlength_FLS_I(field_characteristics, new_borehole_characteristics, tmax, dT_max));
    @test  res1 == "\n\nOBS! The underground temperature change will exceed the threshold even without the current installation\n\nThe maximum temperature change without the new installation will be: 5.71 degrees\n\nThe maximum temperature change with the new installation will be: 5.74 degrees.";

    "2nd case: an insanely long borehole would be necessary to respect the temperature threshold"
    dT_max = 5.72;
    res2 = @capture_out(minimumlength_FLS_I(field_characteristics, new_borehole_characteristics, tmax, dT_max));
    @test  res2 == "\n\nThe proposed borehole length is not sufficient to respect the imposed temperature threshold. The maximum temperature change with the new proposed installation would be: 5.74 degrees.\n\nThe required borehole length to respect the threshold is: 534 m \n\n";

    "3rd case: it is possible to calculate an optimal length"
    dT_max = 5.74;
    res3 = @capture_out(minimumlength_FLS_I(field_characteristics, new_borehole_characteristics, tmax, dT_max));
    @test res3 =="\n\nThe proposed borehole length is not sufficient to respect the imposed temperature threshold. The maximum temperature change with the new proposed installation would be: 5.74 degrees.\n\nThe required borehole length to respect the threshold is: 214 m \n\n";

    "4th case: the borehole length initially suggested is sufficientt to respect the temperature threshold"
    dT_max = 5.75;
    res4 = @capture_out(minimumlength_FLS_I(field_characteristics, new_borehole_characteristics, tmax, dT_max));
    @test  res4 == "\n\nCongrats! The proposed borehole length is sufficient to respect the temperature threshold\n\nThe maximum temperature change without the new installation will be: 5.71 degrees\n\nThe maximum temperature change with the new installation will be: 5.74 degrees.";
