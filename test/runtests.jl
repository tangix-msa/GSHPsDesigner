using Test
using GSHPsDesigner

using Statistics
using Suppressor
using JLD2

include("test_hiddenfunctions.jl")
include("test_FLS_I.jl")
include("test_FLS_III.jl")
include("test_FLS_IV.jl")
# include("test_grids.jl") # test currently fails
include("test_minimumtemperature.jl")
# include("test_minimumlength.jl") # test currently fails
# include("test_sustainabletime.jl") # test currently fails
include("test_groundheatpump.jl")
